
#ifndef MDUILIB_GEOMETRY_DATAS_MBITMAP_H
#define MDUILIB_GEOMETRY_DATAS_MBITMAP_H

#include"../Utils/Utils.h"
#include"../Utils/MString.h"
#include<vector>
namespace MDuilib
{
	/*
	*	@ClassName:MColorARGB
	*	@Remark:ARGB颜色模型表示
	*/
	typedef struct MCOLOR_RGBA
	{
		MByte R;
		MByte G;
		MByte B;
		MByte A;
	} MColor;

	/*
	*	@ClassName:MBitmap
	*	@Attribute:m_Width
	*	@Attribute:m_Height
	*	@Attribute:m_Pitch
	*	@Attribute:m_BPP[8,24,32]
	*	@Attribute:m_Pixels
	*/
	class MBitmap;
	class MBitmap
	{
	private:
		int m_Width;
		int m_Height;
		int m_Pitch;
		int m_BPP;
		MByte* m_Pixels;
	public:
		MBitmap();
		//@Remark:从文件中读取图片
		MBitmap(const String &imgName);
		//@Remark:创建空的Bitmap
		MBitmap(int width, int height, int chanels = 4);
		MBitmap(const MBitmap &other);
		MBitmap(MBitmap &&other);
		MBitmap &operator=(const MBitmap& other);
		MBitmap &operator=(MBitmap &&other);
		~MBitmap();
		MByte* GetDatas() const;
		int GetDataSize() const;
		
		int GetWidth() const;
		int GetHeight() const;
		int GetPitch() const;
		int GetBPP() const;

		template<typename PixelType>
		PixelType* operator()(int x, int y)
		{
			MDUILIB_ASSERT(x > 0 && x < GetWidth() && y>0 && y < GetWidth());
			return static_cast<PixelType*>(x*GetPitch() + y * sizeof(PixelType));
		}

		void Swap(MBitmap &other);
	};
}
#endif // !MDUILIB_GEOMETRY_DATAS_MBITMAP_H
