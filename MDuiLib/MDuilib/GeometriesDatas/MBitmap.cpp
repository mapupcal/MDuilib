
#include"MBitmap.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ASSERT(x)
#include"../ThirdParty/stb_image.h"
namespace MDuilib
{
	MBitmap::MBitmap()
	{
	}
	MBitmap::MBitmap(const String & imgName)
	{
		int n = 0;
		m_Pixels = stbi_load(imgName.ToStdString().c_str(), &m_Width, &m_Height, &n, 4);
		MDUILIB_ASSERT_MSG(m_Pixels != nullptr, "����ͼƬʧ�ܣ�");
		m_BPP = n * 8;
		m_Pitch = m_Width * n;
	}
	MBitmap::MBitmap(int width, int height, int chanels)
		:m_Width(width), m_Height(height)
	{
		MDUILIB_ASSERT(width > 0 && height > 0);
		m_BPP = chanels * 8;
		MDUILIB_ASSERT(m_BPP == 8 || m_BPP == 24 || m_BPP == 32);
		m_Pitch = m_Width * chanels;
		m_Pixels = static_cast<MByte*>(malloc(m_Height*m_Pitch));
		MDUILIB_ASSERT(m_Pixels != nullptr);
	}
	MBitmap::MBitmap(const MBitmap & other)
	{
		m_Width = other.m_Width;
		m_Height = other.m_Height;
		m_Pitch = other.m_Pitch;
		m_BPP = other.m_BPP;
		m_Pixels = static_cast<MByte*>(malloc(other.GetDataSize()));
		MDUILIB_ASSERT(m_Pixels != nullptr);
		auto r = memcpy_s(m_Pixels, other.GetDataSize(), other.m_Pixels, other.GetDataSize());
		MDUILIB_ASSERT(r == 0);
	}
	MBitmap::MBitmap(MBitmap && other)
	{
		m_Width = other.m_Width;
		m_Height = other.m_Height;
		m_Pitch = other.m_Pitch;
		m_BPP = other.m_BPP;
		m_Pixels = other.m_Pixels;
		other.m_Pixels = nullptr;
	}
	MBitmap & MBitmap::operator=(const MBitmap & other)
	{
		MBitmap temp(other);
		Swap(temp);
		return *this;
	}
	MBitmap & MBitmap::operator=(MBitmap && other)
	{
		MBitmap temp(other);
		Swap(temp);
		return *this;
	}
	MBitmap::~MBitmap()
	{
		if (m_Pixels)
			free(m_Pixels);
	}
	MByte * MBitmap::GetDatas() const
	{
		return m_Pixels;
	}
	int MBitmap::GetDataSize() const
	{
		return m_Pitch*m_Height;
	}
	int MBitmap::GetWidth() const
	{
		return m_Width;
	}
	int MBitmap::GetHeight() const
	{
		return m_Height;
	}
	int MBitmap::GetPitch() const
	{
		return m_Pitch;
	}
	int MBitmap::GetBPP() const
	{
		return m_BPP;
	}
	void MBitmap::Swap(MBitmap & other)
	{
		std::swap(m_Width, other.m_Width);
		std::swap(m_Height, other.m_Height);
		std::swap(m_Pitch, other.m_Pitch);
		std::swap(m_BPP, other.m_BPP);
		std::swap(m_Pixels, other.m_Pixels);
	}
}