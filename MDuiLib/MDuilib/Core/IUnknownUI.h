
#ifndef MDUILIB_CORE_IUNKNOWN_UI_H
#define MDUILIB_CORE_IUNKNOWN_UI_H

/*
*	@Remark:所有UI对象的基类
*/
#include"../Utils/MString.h"

namespace MDuilib
{
	class IUnknownUI
	{
	public:
		/*
		*	@Remark:获取派生类的类型字符串。
		*/
		virtual const String GetClassName() const = 0;
		/*
		*	@Remark:查询该对象是否具备输入类名的接口。用于指针向下转型，配合GetClassName，使用者必须知道继承链。
		*	@Noted:	比如，有继承链 IUnknownUI <- IControl <- Button.
		*			我们获得了一个IUnknown类型的指针pObj（指向Button），先通过GetClassName获得该派生类类型字符串(maybe. "MDUILIB_BUTTON")，在继承链中-
		*			我们想要使用该对象的IControl(maybe. "MDUILIB_ICONTROL")的一个SetText接口，则利用以下语法：
		*			IControl* pCtrl = static_cast<IControl*>(pOj->GetInterface("MDUILIB_ICONTROL"))
		*			pCtrl->SetText("XXXX");
		*/
		virtual IUnknownUI* GetInterface(const String &className) const = 0;

		void SetName(const String& objName)
		{
			m_Name = objName;
		}
		const String GetName() const
		{
			return m_Name;
		}
	private:
		String m_Name;
	};
}

#endif // !MDUILIB_CORE_OBJECT_UI_H
