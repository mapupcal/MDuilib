
#ifndef MDUILIB_CORE_PLATFORM_SPECIFIC_MWINDOWS_H
#define MDUILIB_CORE_PLATFORM_SPECIFIC_MWINDOWS_H

/*
*	@Remark:定义与平台相关的Window类型
*/
#include"IWindow.h"
#include"../PlatformSpecific/Utils.h"

namespace MDuilib
{
#if MDUILIB_WINVERSION_WIN7_OR_PLUS
	class CWindowForWin7OrPlus : public IWindow
	{
	public:
		friend class CRenderWin7OrPlus;
	public:
		using WindowsListType = std::vector<IWindow*>;
		// Inherited via IWindow
		virtual void InitWindow(const String & wndTittleName, const MRect & positionRect) override;
		virtual IWindow* CreateSubWindow(const String & subWndTittleName, const MRect & relativePositionRect, bool bModal = false) override;
		virtual void SetTittle(const String & wndTittleName) override;
		virtual String GetTittle() const override;
		virtual void Show() override;
		virtual void Resize(MUINT width, MUINT height) override;
		virtual void Maximize() override;
		virtual void Minimize() override;
		virtual void CenterWindow() override;
		virtual void Close() override;
		virtual void Move(MUINT pX, MUINT pY) override;
		virtual IWindow::HandleType GetNativeWindowHandle() const override;
		virtual IWindow::HandleType GetNativeRenderTarget() const override;
		virtual bool IsModal() const override;
		virtual void Jitter() override;
		CWindowForWin7OrPlus();
		virtual ~CWindowForWin7OrPlus();
		static LRESULT CALLBACK __WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	private:
		HRESULT __Initialize(HWND hParent,const String &wndTittleName, const MRect& positionRect);
		HRESULT __CreateD2DDeviceIndependentResources();
		HRESULT __CreateD2DDeviceResources();
		void __ReleaseResources();
	private:
		HWND m_hWnd;
		bool m_bModal;
		String m_WndTittle;
		//	@Remark:设备无关的D2D资源
		ID2D1Factory *m_pD2d1Factory;
		IDWriteFactory *m_pDWriteFactory;

		ID2D1HwndRenderTarget *m_pRenderTarget;
		WindowsListType m_ModalSubWnds;
		WindowsListType m_ModalessSubWnds;
	};

#endif // MDUILIB_WINVERSION_WIN7_OR_PLUS

}

#endif // !MDUILIB_CORE_PLATFORM_SPECIFIC_MWINDOWS_H
