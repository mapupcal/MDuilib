
#ifndef MDUILIB_CORE_PLATFORM_SPECIFIC_IWINDOW_H
#define MDUILIB_CORE_PLATFORM_SPECIFIC_IWINDOW_H

/*
*	@Remark:定义与平台无关的窗口接口
*/
#include"../../Utils/MString.h"
#include"../../Utils/MDelegate.h"
#include"../../GeometriesDatas/MRect.h"
#include"../../Core/Events.h"
namespace MDuilib
{
	class IWindow
	{
	public:
		/*
		*	@Attribute:HandleType,与平台无关的句柄类型，为void*
		*/
		using HandleType = void*;
		/*
		*	@Attribute：WindowProcNotifyers
		*	@Remark:向相应的事件注册Notifyer，窗口发生相应事件时，将通知Notifyer。
		*	@Noted:注册的Notifyer必须具备Callable接口void(IWindows*,MEvent&),See Also class MEvent.
		*/
		using WindowProcNotifyers = DelegateNotifyers<IWindow*, MEvent&>;
	public:

		virtual ~IWindow() {}
		virtual void InitWindow(const String &wndTittleName, const MRect &positionRect) = 0;
		virtual IWindow* CreateSubWindow(const String &subWndTittleName,\
			const MRect& relativePositionRect,bool bModal = false) = 0;
		virtual void SetTittle(const String& wndTittleName) = 0;
		virtual String GetTittle() const = 0;
		virtual void Show() = 0;
		virtual void Resize(MUINT width, MUINT height) = 0;
		virtual void Maximize() = 0;
		virtual void Minimize() = 0;
		virtual void CenterWindow() = 0;
		virtual void Close() = 0;
		virtual void Move(MUINT pX, MUINT pY) = 0;
		virtual HandleType GetNativeWindowHandle() const = 0;
		virtual HandleType GetNativeRenderTarget() const = 0;
		virtual bool IsModal() const = 0;
		/*
		*	@Remark:获取矩形位置
		*/
		const MRect::data_type GetX() const
		{
			return GetWndRect().left;
		}
		const MRect::data_type GetY() const
		{
			return GetWndRect().top;
		}
		const MRect::data_type GetClientX() const
		{
			return GetClientRect().left;
		}
		const MRect::data_type GetClientY() const
		{
			return GetClientRect().top;
		}
		/*
		*	@Remark:获取窗口矩形属性、Client矩形属性。
		*/
		const MRect GetWndRect() const
		{
			return m_WindowRect;
		}
		const MRect GetClientRect() const
		{
			return m_ClientRect;
		}
		MRect::data_type GetWidth() const
		{
			return GetRectWidth(GetWndRect());
		}
		MRect::data_type GetHeigth() const
		{
			return GetRectHeight(GetWndRect());
		}
		MRect::data_type GetClientWidth()  const
		{
			return GetRectWidth(GetClientRect());
		}
		MRect::data_type GetClientHeight()  const
		{
			return GetRectHeight(GetClientRect());
		}
		/*
		*	@FunctionName:Jitter
		*	@Remark:抖动窗口。
		*/
		virtual void Jitter() = 0;
		//	@Remark:WindowNotifyers
		WindowProcNotifyers OnInit;
		WindowProcNotifyers OnClose;
		WindowProcNotifyers OnSize;
		WindowProcNotifyers OnMove;
		WindowProcNotifyers OnMessage;

	protected:
		MRect m_WindowRect;
		MRect m_ClientRect;
	};
}

#endif // !MDUILIB_CORE_PLATFORM_SPECIFIC_IWINDOW_H
