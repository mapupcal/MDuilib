

#ifndef MDUILIB_CORE_PLATFORM_SPECIFIC_UTILS_H
#define MDUILIB_CORE_PLATFORM_SPECIFIC_UTILS_H

#include"../../Utils/Utils.h"
#include"../../GeometriesDatas/MRect.h"
#include"../../GeometriesDatas/MBitmap.h"
//	@Remark:For PlatformSpecific
#ifdef _WIN32	
	#include<Windows.h>
	#include<windowsx.h>
	#define MDUILIB_WINVERSION_WIN7_OR_PLUS (_WIN32_WINNT >= 0x601 && WINVER>=0x601)
namespace MDuilib
{
	//@Remark:From System's RECT Structure to MRect
	MRect ConvertRECTToMRect(const RECT &rc);
	RECT ConvertToRECT(const MRect& rc);
}
	//Win7 + using D2D
	#if MDUILIB_WINVERSION_WIN7_OR_PLUS
		//	@Remark:Header Files For D2D
		#include <stdlib.h>
		#include <malloc.h>
		#include <memory.h>
		#include <wchar.h>
		#include <math.h>
		#include <d2d1.h>
		#include <d2d1helper.h>
		#include <dwrite.h>
		#include <wincodec.h>
		#pragma comment(lib,"d2d1.lib")
		#pragma comment(lib,"dwrite.lib")
		#ifndef HINST_THISCOMPONENT
		EXTERN_C IMAGE_DOS_HEADER __ImageBase;
		#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)

		namespace MDuilib
		{
			//	@Remark:安全释放Com组件
			template<typename Interface>
			inline void SafeRelease(Interface **ppInterfaceToRelease)
			{
				if (*ppInterfaceToRelease != NULL)
				{
					(*ppInterfaceToRelease)->Release();
					(*ppInterfaceToRelease) = NULL;
				}
			}

			//	@Remark:定义一些关于MXXXX数据结构和D2D1数据结构的转换。
			D2D1_COLOR_F ConvertToD2D1_COLOR_F(const MColor& color);
			D2D1_RECT_F ConvertToD2D1_RECT_F(const MRect &rect);
		}

		#endif
	//TODO： Win7 - using GDI+ 
	#else
	#endif	//MDUILIB_WINVERSION_WIN7_OR_PLUS
#endif // _WIN32

#endif	//MDUILIB_CORE_PLATFORM_SPECIFIC_UTILS_H