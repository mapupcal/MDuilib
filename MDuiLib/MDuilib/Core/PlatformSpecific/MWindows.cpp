#include "MWindows.h"

namespace MDuilib
{
#if MDUILIB_WINVERSION_WIN7_OR_PLUS

	void CWindowForWin7OrPlus::InitWindow(const String & wndTittleName, const MRect & positionRect)
	{
		HRESULT hr = __Initialize(NULL,wndTittleName, positionRect);
		MDUILIB_ASSERT_MSG(SUCCEEDED(hr), "初始化窗口失败！");
	}

	IWindow * CWindowForWin7OrPlus::CreateSubWindow(const String & subWndTittleName, const MRect & relativePositionRect, bool bModal)
	{
		CWindowForWin7OrPlus *pSubWindow = new CWindowForWin7OrPlus();
		MRect posRect = Translate(relativePositionRect, m_WindowRect.left, m_WindowRect.top);
		pSubWindow->__Initialize(m_hWnd, subWndTittleName, posRect);
		pSubWindow->m_bModal = bModal;
		if (bModal)
		{
			m_ModalSubWnds.push_back(pSubWindow);
		}
		else
		{
			m_ModalessSubWnds.push_back(pSubWindow);
		}
		//	@Commit:当窗口关闭的时候，移除该子窗口的指针。
		pSubWindow->OnClose += [&](IWindow *pWindow, MEvent &e)
		{
			auto p = std::find(this->m_ModalSubWnds.begin(), this->m_ModalSubWnds.end(), pWindow);
			if (p != this->m_ModalSubWnds.end())
			{
				this->m_ModalSubWnds.erase(p);
			}
			p = std::find(this->m_ModalessSubWnds.begin(), this->m_ModalessSubWnds.end(), pWindow);
			if (p != this->m_ModalessSubWnds.end())
			{
				this->m_ModalessSubWnds.erase(p);
			}
		};
		return pSubWindow;
	}

	void CWindowForWin7OrPlus::SetTittle(const String & wndTittleName)
	{
		std::string tittle = wndTittleName.ToStdString();
		m_WndTittle = wndTittleName;
		::SetWindowTextA(m_hWnd,tittle.c_str());
	}

	String CWindowForWin7OrPlus::GetTittle() const
	{
		return m_WndTittle;
	}

	void CWindowForWin7OrPlus::Show()
	{
		if (m_hWnd != NULL)
		{
			::ShowWindow(m_hWnd, SW_SHOWNORMAL);
			::UpdateWindow(m_hWnd);
		}
		else
		{
			MDUILIB_OUT_ERROR("窗体尚未初始化或初始化失败！");
		}
	}

	void CWindowForWin7OrPlus::Resize(MUINT width, MUINT height)
	{
		::MoveWindow(m_hWnd, m_WindowRect.left, m_WindowRect.top, width, height, true);
	}

	void CWindowForWin7OrPlus::Maximize()
	{
		if (m_hWnd)
		{
			::ShowWindow(m_hWnd, SW_MAXIMIZE);
			::UpdateWindow(m_hWnd);
		}
		else
		{
			MDUILIB_OUT_ERROR("窗体句柄未初始化或初始化失败！");
		}
	}

	void CWindowForWin7OrPlus::Minimize()
	{
		if (m_hWnd)
		{
			::ShowWindow(m_hWnd, SW_MINIMIZE);
		}
		else
		{
			MDUILIB_OUT_ERROR("窗体句柄未初始化或初始化失败！");
		}
	}

	void CWindowForWin7OrPlus::CenterWindow()
	{
		//	@TODO:如何解决多屏幕问题。
		HWND hDesktop = ::GetDesktopWindow();
		RECT rc;
		::GetWindowRect(hDesktop, &rc);
		auto x = ((rc.right - rc.left) - GetRectWidth(m_WindowRect)) / 2;
		auto y = ((rc.bottom - rc.top) - GetRectHeight(m_WindowRect)) / 2;
		Move(x, y);
	}

	void CWindowForWin7OrPlus::Close()
	{
		if (m_hWnd != NULL)
		{
			::DestroyWindow(m_hWnd);
			m_hWnd = NULL;
		}
	}

	void CWindowForWin7OrPlus::Move(MUINT pX, MUINT pY)
	{
		auto xOffset = m_WindowRect.left - pX;
		auto yOffset = m_WindowRect.top - pY;
		//	@Commit:更改m_WindowRect将在WndProc里面。
		::MoveWindow(m_hWnd, pX, pY, \
			GetRectWidth(m_WindowRect), GetRectHeight(m_WindowRect), false);
	}

	IWindow::HandleType CWindowForWin7OrPlus::GetNativeWindowHandle() const
	{
		return static_cast<IWindow::HandleType>(m_hWnd);
	}

	IWindow::HandleType CWindowForWin7OrPlus::GetNativeRenderTarget() const
	{
		return static_cast<IWindow::HandleType>(m_pRenderTarget);
	}

	bool CWindowForWin7OrPlus::IsModal() const
	{
		return m_bModal;
	}


	void CWindowForWin7OrPlus::Jitter()
	{
		//	@Remark:硬编码的窗口抖动动画。
		//	@Commit:最好适宜发送消息的方式通知自己需要被抖动。
		//	::SendMessageA(m_hWnd, WINDOW_JITTER_MESSAGE, NULL, NULL);
		//	TODO:
	}

	CWindowForWin7OrPlus::CWindowForWin7OrPlus()
		:m_hWnd(NULL),
		m_pD2d1Factory(NULL),
		m_pRenderTarget(NULL),
		m_pDWriteFactory(NULL),
		m_bModal(false)
	{

	}
	CWindowForWin7OrPlus::~CWindowForWin7OrPlus()
	{
		Close();
		//	@Remark:关闭所有的IWindow
		//	@Commint:子窗口的指针资源应该由用户delete.
	}
	LRESULT CALLBACK CWindowForWin7OrPlus::__WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		LRESULT hr = 0;
		if (message == WM_CREATE)
		{
			//	@Commit:将IWindow与hWnd相关联。
			LPCREATESTRUCTA pcs = (LPCREATESTRUCTA)lParam;
			CWindowForWin7OrPlus*pWindow = static_cast<CWindowForWin7OrPlus*>(pcs->lpCreateParams);
			::SetWindowLongPtrA(
				hWnd,
				GWLP_USERDATA,
				PtrToLong(pWindow)
			);
			hr = 1;
		}
		else
		{
			//	@Commit:获取和hWnd相关联的IWindow指针。
			CWindowForWin7OrPlus* pWindow = reinterpret_cast<CWindowForWin7OrPlus*>(
				static_cast<LONG_PTR>(
					::GetWindowLongPtrA(
						hWnd,
						GWLP_USERDATA
					)
					)
				);

			//	@Commit:处理其它消息。
			bool wasHandled = false;
			if (pWindow)
			{
				switch (message)
				{
				//	@Commit:窗口移动
				case WM_MOVE:
				{
					RECT rc;
					//	@Commit:获取当前窗口的区域位置。
					::GetWindowRect(hWnd, &rc);
					//	@Commit：设置当前的m_WindowRect。
					MRect& rect = pWindow->m_WindowRect;
					rect = ConvertRECTToMRect(rc);
					//	@Commit：通知OnMove注册的Notifyers
					MEvent e;
					e.Type = MEventType::EVENT_WINDOW_MOVE;
					e.lParam = &rect;
					for (const auto &handler : pWindow->OnMove)
						handler(pWindow, e);
					wasHandled = true;
				}
					break;

				//	@Commit:窗口缩放
				case WM_SIZE:
				{
					RECT rc;
					//	@Commit:获取当前Window位置。
					::GetWindowRect(hWnd, &rc);
					//	@Commit:修改m_WindowRect。
					MRect& rect = pWindow->m_WindowRect;
					rect = ConvertRECTToMRect(rc);
					//	@Commit:获取Client的区域。
					::GetClientRect(hWnd, &rc);
					pWindow->m_ClientRect = ConvertRECTToMRect(rc);
					//	@Commit:通知OnSize注册的Notifyers
					MEvent e;
					e.Type = MEventType::EVENT_WINDOW_SIZE;
					e.lParam = &rect;
					for (const auto& handler : pWindow->OnSize)
						handler(pWindow, e);
					//	@Commit:设置渲染目标的Resize相关
					D2D1_SIZE_U targetRc;
					targetRc.height = rc.bottom - rc.top;
					targetRc.width = rc.right - rc.left;
					pWindow->m_pRenderTarget->Resize(targetRc);
					wasHandled = true;
				}
					break;
				//	@Commit:窗口需要被重新绘制
				case WM_PAINT:
				{
					//	@Commit:重新绘制窗口由外部Handler负责。
					MEvent e;
					e.Type = MEventType::EVENT_WINDOW_PAINT;
					for (const auto &handler : pWindow->OnMessage)
						handler(pWindow, e);
					ValidateRect(hWnd, NULL);
					wasHandled = true;
				}
				break;
				//	@Commit:鼠标移动操作
				case WM_MOUSEMOVE:
				//	@Commit:鼠标停留
				case WM_MOUSEHOVER:
				{
					auto mouseX = GET_X_LPARAM(lParam);
					auto mouseY = GET_Y_LPARAM(lParam);
					MEvent e;
					if (message == WM_MOUSEMOVE)
					{
						e.Type = MEventType::EVENT_MOUSE_MOVE;
					}
					else if (message == WM_MOUSEHOVER)
					{
						e.Type = MEventType::EVENT_MOUSE_HOVER;
					}
					e.ptMouse.x = mouseX;
					e.ptMouse.y = mouseY;
					//TODO:Other Attribute in Event
					for (const auto & handler : pWindow->OnMessage)
						handler(pWindow, e);
					wasHandled = true;
				}
					break;
				//	@Commit:字符输入
				//	@Remark:统一转换成UTF16编码的字符返回，其UTF16编码的字符保存在e.chKey中
				case WM_CHAR:
				{
					wasHandled = true;
					MEvent e;
					e.Type = MEventType::EVENT_CHAR;
					//	@Remark:标记该输入字符是否为多字节编码的字符
					//	@Buggy:会不会造成多窗口的并发问题？
					static bool multiBytes = false;
					static char bytes[2];
					unsigned char byte = (unsigned char)(wParam);
					if (multiBytes)
					{
						//	@Commit:多字节输入
						bytes[1] = byte;
						wchar_t ch;
						::MultiByteToWideChar(::GetACP(), 0, bytes, 2, &ch, 1);
						e.chKey = ch;
						multiBytes = false;
					}
					else 
					{
						bytes[0] = byte;
						if (byte <= 127)
						{
							e.chKey = byte;
						}
						else
						{
							multiBytes = true;
							break;
						}
					}

					for (const auto &handler : pWindow->OnMessage)
						handler(pWindow, e);
				}
					break;
				//	@Commit:鼠标左键按下
				case WM_LBUTTONDOWN:
				//	@Commit:鼠标左键松开
				case WM_LBUTTONUP:
				//	@Commit:鼠标右键按下
				case WM_RBUTTONDOWN:
				//	@Commit:鼠标右键松开
				case WM_RBUTTONUP:
				{
					MEvent e;
					if (WM_LBUTTONDOWN == message)
					{
						e.Type = MEventType::EVENT_LBUTTON_DOWN;
					}
					else if (WM_LBUTTONUP == message)
					{
						e.Type = MEventType::EVENT_LBUTTON_UP;
					}
					else if (WM_RBUTTONDOWN == message)
					{
						e.Type = MEventType::EVENT_RBUTTON_DOWN;
					}
					else
					{
						e.Type = MEventType::EVENT_RBUTTON_UP;
					}
					auto mouseX = GET_X_LPARAM(lParam);
					auto mouseY = GET_Y_LPARAM(lParam);
					e.ptMouse.x = mouseX;
					e.ptMouse.y = mouseY;
					for (const auto& handler : pWindow->OnMessage)
						handler(pWindow, e);
					wasHandled = true;
				}
				break;
				//	@Commit:窗口接收到关闭消息
				case WM_CLOSE:
				{
					//	@Commit:该窗口具备模态子窗口，抖动所有模态窗口以通知用户。
					if (!pWindow->m_ModalSubWnds.empty())
					{
						for (auto &w : pWindow->m_ModalSubWnds)
						{
							w->Jitter();
						}
						wasHandled = true;
					}
				}
				break;
				//	@Commit:窗口被销毁。
				case WM_DESTROY:
				{
					MEvent e;
					for (const auto&handler : pWindow->OnClose)
						handler(pWindow, e);
					pWindow->__ReleaseResources();
					wasHandled = true;
				}
					break;
				case WM_DISPLAYCHANGE:
				{
					InvalidateRect(hWnd, NULL, FALSE);
					hr = 0;
					wasHandled = true;
				}

				break;
				default:
					break;
				}
			}
			if (!wasHandled)
			{
				hr =  ::DefWindowProcA(hWnd, message, wParam, lParam);
			}
		}
		return hr;
	}
	HRESULT CWindowForWin7OrPlus::__Initialize(HWND hParent,const String & wndTittleName, const MRect & positionRect)
	{
		HRESULT hr = S_OK;
		//	@Commit:初始化m_pD2d1Factory
		hr = __CreateD2DDeviceIndependentResources();
		if (SUCCEEDED(hr))
		{
			//	@Commit:注册Win平台的 window class.
			WNDCLASSEXA wcex = { sizeof(WNDCLASSEXA) };
			wcex.style = CS_HREDRAW | CS_VREDRAW;
			wcex.lpfnWndProc = CWindowForWin7OrPlus::__WndProc;
			wcex.cbClsExtra = 0;
			wcex.cbWndExtra = sizeof(LONG_PTR);
			wcex.hInstance = NULL;
			wcex.hbrBackground = NULL;
			wcex.lpszMenuName = NULL;
			wcex.hCursor = LoadCursorA(NULL, IDI_APPLICATION);
			wcex.lpszClassName = "MDUILIB_WINDOW";

			RegisterClassExA(&wcex);

			//	@Commit 获取系统的DPI
			FLOAT dpiX, dpiY;
			m_pD2d1Factory->GetDesktopDpi(&dpiX, &dpiY);
			
			//	@Commit:创建window句柄
			auto tittle = wndTittleName.ToStdString();
			m_hWnd = CreateWindowA(
				"MDUILIB_WINDOW",
				tittle.c_str(),
				WS_OVERLAPPEDWINDOW,
				static_cast<UINT>(positionRect.left),
				static_cast<UINT>(positionRect.top),
				static_cast<UINT>(ceil(GetRectWidth(positionRect)*dpiX / 96.0f)),
				static_cast<UINT>(ceil(GetRectHeight(positionRect)*dpiY / 96.0f)),
				hParent,
				NULL,
				NULL,
				this
			);
			//@	Commit:将此对象指针与m_hWnd相关联
			hr = m_hWnd ? S_OK : E_FAIL;
			MDUILIB_ASSERT_MSG(SUCCEEDED(hr), "创建窗口失败！");
			m_WindowRect = positionRect;
			//	@Commit:创建D2D设备相关的资源
			hr =__CreateD2DDeviceResources();
			MDUILIB_ASSERT_MSG(SUCCEEDED(hr), "创建设备相关的资源失败！");
		}
		return hr;
	}
	HRESULT CWindowForWin7OrPlus::__CreateD2DDeviceIndependentResources()
	{
		HRESULT hr = S_OK;
		hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2d1Factory);
		MDUILIB_ASSERT_MSG(SUCCEEDED(hr), "创建D2D1工厂失败！");
		hr = DWriteCreateFactory(
			DWRITE_FACTORY_TYPE_SHARED,
			__uuidof(IDWriteFactory),
			reinterpret_cast<IUnknown**>(&m_pDWriteFactory)
		);
		return hr;
	}
	HRESULT CWindowForWin7OrPlus::__CreateD2DDeviceResources()
	{
		HRESULT hr = S_OK;
		if (!m_pRenderTarget)
		{
			RECT rc;
			::GetClientRect(m_hWnd, &rc);
			D2D1_SIZE_U size = D2D1::SizeU(
				rc.right - rc.left,
				rc.bottom - rc.top
			);
			//	@Commit:初始化m_pRenderTarget
			hr = m_pD2d1Factory->CreateHwndRenderTarget(
				D2D1::RenderTargetProperties(),
				D2D1::HwndRenderTargetProperties(m_hWnd, size),
				&m_pRenderTarget
			);
			MDUILIB_ASSERT_MSG(SUCCEEDED(hr), "D2D初始化窗口渲染目标失败！");
		}
		return hr;
	}
	void CWindowForWin7OrPlus::__ReleaseResources()
	{
		//	@Remark:释放从系统获取到的资源
		SafeRelease(&m_pD2d1Factory);
		SafeRelease(&m_pDWriteFactory);
		SafeRelease(&m_pRenderTarget);
	}
#endif // MDUILIB_WINVERSION_WIN7_OR_PLUS
}
