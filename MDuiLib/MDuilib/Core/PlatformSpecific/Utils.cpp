#include"Utils.h"

#ifdef _WIN32
namespace MDuilib
{
	MRect ConvertRECTToMRect(const RECT & rc)
	{
		MRect rect;
		rect.top = static_cast<MRect::data_type>(rc.top);
		rect.bottom = static_cast<MRect::data_type>(rc.bottom);
		rect.left = static_cast<MRect::data_type>(rc.left);
		rect.right = static_cast<MRect::data_type>(rc.right);
		return rect;
	}
	RECT ConvertToRECT(const MRect & rc)
	{
		RECT rect;
		rect.top = rc.top;
		rect.bottom = rc.bottom;
		rect.left = rc.left;
		rect.right = rc.right;
		return rect;
	}
#if MDUILIB_WINVERSION_WIN7_OR_PLUS
	D2D1_COLOR_F ConvertToD2D1_COLOR_F(const MColor & color)
	{
		D2D1_COLOR_F col;
		col.a = color.A / 255.0f;
		col.r = color.R;
		col.b = color.B;
		col.g = color.G;
		return col;
	}
	D2D1_RECT_F ConvertToD2D1_RECT_F(const MRect & rect)
	{
		D2D1_RECT_F rc;
		rc.top = rect.top;
		rc.left = rect.left;
		rc.right = rect.right;
		rc.bottom = rect.bottom;
		return rc;
	}
#endif
}
#endif// _WIN32
