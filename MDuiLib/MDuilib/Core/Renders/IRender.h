
#ifndef MDUILIB_CORE_RENDERS_IRENDER_H
#define MDUILIB_CORE_RENDERS_IRENDER_H

/*
*	@Remark:定义与平台无关的渲染接口。所有平台的渲染实现都必须继承这个接口。
*/
#include"../../GeometriesDatas/MRect.h"
#include"../../GeometriesDatas/MBitmap.h"
#include"../PlatformSpecific/IWindow.h"
namespace MDuilib
{
	class IRender
	{
	public:
		//	@Remark:画或填充矩形。
		virtual void DrawRect(IWindow *pWnd, const MRect& rect, const MColor& color, int lineStyle) = 0;
		virtual void FillRect(IWindow *pWnd, const MRect& rect, const MColor& color) = 0;
		//	@Remark:圆角矩形。
		virtual void DrawRoundedRect(IWindow *pWnd, const MRect& rect, const MColor& color,\
			float xRadius, float yRadius, int lineStyle) = 0;
		virtual void FillRoundedRect(IWindow *pWnd, const MRect& rect, const MColor& color, \
			float xRadius, float yRadius) = 0;
		//	@Remark:图片。
		virtual void DrawImage(IWindow* pWnd, const MBitmap &bitmap, const MRect &targetRect,\
			float fadeLevel = 1.0f, bool bUseAlpha = false) = 0;
		//	@Remark:带颜色梯度的矩形。
		virtual void DrawGradientRect(IWindow* pWnd, const MRect& rect, \
			const MColor& firstColor, const MColor &secondColor, \
			int lineStyle, bool bRadius = false
		) = 0;
		virtual void FillGradientRect(IWindow* pWnd, const MRect& rect, \
			const MColor& firstColor, const MColor &secondColor, \
			bool bRadius = false
		) = 0;
		//	@Remark:带颜色梯度的圆角矩形。
		virtual void DrawGradientRoundedRect(IWindow* pWnd, const MRect& rect, \
			float xRadius, float yRadius, \
			const MColor& firstColor, const MColor &secondColor, \
			int lineStyle, bool bRadius = false
		) = 0;
		virtual void FillGradientRoundedRect(IWindow* pWnd, const MRect& rect, \
			float xRadius, float yRadius, \
			const MColor& firstColor, const MColor &secondColor, \
			bool bRadius = false
		) = 0;
		//	@Remark:渲染文本。
		virtual void DrawTextA(IWindow *pWnd, const AString& text, const MRect &rect,const MColor &color,int nSize,int Font) = 0;
		virtual void DrawTextW(IWindow *pWnd, const WString& text, const MRect &rect, const MColor &color,int nSize,int Font) = 0;
		//	@Remark:计算渲染文本所需的矩形大小。
		virtual const MRect CalculateTextA(IWindow *pWnd, const AString& text, int nSize,int Font) = 0;
		virtual const MRect CalculateTextW(IWindow *pWnd, const WString& text, int nSize,int Font) = 0;
		//	@Remark:从窗体中获取对应位置的矩形位图。
		virtual MBitmap GenerateBitmap(IWindow *pWnd, const MRect &targetRect) = 0;
	};
}

#endif // !MDUILIB_CORE_RENDERS——IRENDER_H
