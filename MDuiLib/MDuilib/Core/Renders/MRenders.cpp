#include "MRenders.h"
#include "../PlatformSpecific/MWindows.h"


namespace MDuilib
{
#if MDUILIB_WINVERSION_WIN7_OR_PLUS

	IRender* Render::m_pCRender = new CRenderWin7OrPlus();

	void CRenderWin7OrPlus::DrawRect(IWindow * pWnd, const MRect & rect, const MColor & color, int lineStyle)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1SolidColorBrush* pBrush;

		D2D1_COLOR_F col = ConvertToD2D1_COLOR_F(color);

		D2D1_RECT_F rc = ConvertToD2D1_RECT_F(rect);

		pRenderTarget->CreateSolidColorBrush(col, &pBrush);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		pRenderTarget->DrawRectangle(rc, pBrush);
		pRenderTarget->EndDraw();
		pBrush->Release();
	}
	void CRenderWin7OrPlus::FillRect(IWindow * pWnd, const MRect & rect, const MColor & color)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1SolidColorBrush* pBrush;

		D2D1_COLOR_F col = ConvertToD2D1_COLOR_F(color);

		D2D1_RECT_F rc = ConvertToD2D1_RECT_F(rect);

		pRenderTarget->CreateSolidColorBrush(col, &pBrush);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		pRenderTarget->FillRectangle(rc, pBrush);
		pRenderTarget->EndDraw();
		pBrush->Release();
	}
	void CRenderWin7OrPlus::DrawRoundedRect(IWindow * pWnd, const MRect & rect, const MColor & color, float xRadius, float yRadius, int lineStyle)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1SolidColorBrush* pBrush;

		D2D1_COLOR_F col = ConvertToD2D1_COLOR_F(color);


		D2D1_ROUNDED_RECT rc;
		rc.rect = ConvertToD2D1_RECT_F(rect);
		rc.radiusX = xRadius;
		rc.radiusY = yRadius;

		pRenderTarget->CreateSolidColorBrush(col, &pBrush);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		pRenderTarget->DrawRoundedRectangle(rc, pBrush);
		pRenderTarget->EndDraw();
		pBrush->Release();
	}
	void CRenderWin7OrPlus::FillRoundedRect(IWindow * pWnd, const MRect & rect, const MColor & color, float xRadius, float yRadius)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1SolidColorBrush* pBrush;

		D2D1_COLOR_F col = ConvertToD2D1_COLOR_F(color);


		D2D1_ROUNDED_RECT rc;
		rc.rect = ConvertToD2D1_RECT_F(rect);
		rc.radiusX = xRadius;
		rc.radiusY = yRadius;

		pRenderTarget->CreateSolidColorBrush(col, &pBrush);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		pRenderTarget->FillRoundedRectangle(rc, pBrush);
		pRenderTarget->EndDraw();
		pBrush->Release();
	}

	//TODO:Bug need to fixed.
	void CRenderWin7OrPlus::DrawImage(IWindow * pWnd, const MBitmap & bitmap, const MRect & targetRect, float fadeLevel, bool bUseAlpha)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1Bitmap *d2d1Bitmap;
		D2D1_SIZE_U imgSize;
		imgSize.width = bitmap.GetWidth();
		imgSize.height = bitmap.GetHeight();

		D2D1_BITMAP_PROPERTIES props;
		props.dpiX = 96.0f;
		props.dpiY = 96.0f;
		D2D1_PIXEL_FORMAT pixelFormat = {};
		//暂时只支持支持alpha通道的图片
		if (bitmap.GetBPP() == 32)
		{
			pixelFormat = D2D1::PixelFormat(
				DXGI_FORMAT_R8G8B8A8_UNORM,
				D2D1_ALPHA_MODE_PREMULTIPLIED
			);
		}
		props.pixelFormat = pixelFormat;
		pRenderTarget->CreateBitmap(imgSize, bitmap.GetDatas(), bitmap.GetPitch(), props, &d2d1Bitmap);

		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		auto rc = ConvertToD2D1_RECT_F(targetRect);
		pRenderTarget->DrawBitmap(d2d1Bitmap, rc);
		pRenderTarget->EndDraw();
		d2d1Bitmap->Release();
	}

	void CRenderWin7OrPlus::DrawGradientRect(IWindow * pWnd, const MRect & rect, const MColor & firstColor, const MColor & secondColor, int lineStyle, bool bRadius)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1LinearGradientBrush *pLinearBrush;
		ID2D1RadialGradientBrush *pRadialBrush;
		
		D2D1_GRADIENT_STOP gradientStops[2];
		gradientStops[0].color = ConvertToD2D1_COLOR_F(firstColor);
		gradientStops[0].position = 0.0f;
		gradientStops[1].color = ConvertToD2D1_COLOR_F(secondColor);
		gradientStops[1].position = 1.0f;
		ID2D1GradientStopCollection *pGradientStops;
		pRenderTarget->CreateGradientStopCollection(
			gradientStops,
			2,
			&pGradientStops
		);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		if (!bRadius)
		{
			pRenderTarget->CreateLinearGradientBrush(
				D2D1::LinearGradientBrushProperties(
					D2D1::Point2F(rect.left, rect.top),
					D2D1::Point2F(rect.right, rect.bottom)
				),
				pGradientStops,
				&pLinearBrush
			);
			pRenderTarget->DrawRectangle(ConvertToD2D1_RECT_F(rect), pLinearBrush);
			pLinearBrush->Release();
		}
		else
		{
			pRenderTarget->CreateRadialGradientBrush(
				D2D1::RadialGradientBrushProperties(
					D2D1::Point2F(rect.left + GetRectWidth(rect)/2, rect.top + GetRectHeight(rect)/2),
					D2D1::Point2F(0, 0),
					GetRectWidth(rect),
					GetRectHeight(rect)
				),
				pGradientStops,
				&pRadialBrush
			);
			pRenderTarget->DrawRectangle(ConvertToD2D1_RECT_F(rect), pRadialBrush);
			pRadialBrush->Release();
		}
		pRenderTarget->EndDraw();
	}
	void CRenderWin7OrPlus::FillGradientRect(IWindow * pWnd, const MRect & rect, const MColor & firstColor, const MColor & secondColor, bool bRadius)
	{
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		ID2D1LinearGradientBrush *pLinearBrush;
		ID2D1RadialGradientBrush *pRadialBrush;

		D2D1_GRADIENT_STOP gradientStops[2];
		gradientStops[0].color = ConvertToD2D1_COLOR_F(firstColor);
		gradientStops[0].position = 0.0f;
		gradientStops[1].color = ConvertToD2D1_COLOR_F(secondColor);
		gradientStops[1].position = 1.0f;
		ID2D1GradientStopCollection *pGradientStops;
		pRenderTarget->CreateGradientStopCollection(
			gradientStops,
			2,
			&pGradientStops
		);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		if (!bRadius)
		{
			pRenderTarget->CreateLinearGradientBrush(
				D2D1::LinearGradientBrushProperties(
					D2D1::Point2F(rect.left, rect.top),
					D2D1::Point2F(rect.right, rect.bottom)
				),
				pGradientStops,
				&pLinearBrush
			);
			pRenderTarget->FillRectangle(ConvertToD2D1_RECT_F(rect), pLinearBrush);
			pLinearBrush->Release();
		}
		else
		{
			pRenderTarget->CreateRadialGradientBrush(
				D2D1::RadialGradientBrushProperties(
					D2D1::Point2F(rect.left + GetRectWidth(rect) / 2, rect.top + GetRectHeight(rect) / 2),
					D2D1::Point2F(0, 0),
					GetRectWidth(rect),
					GetRectHeight(rect)
				),
				pGradientStops,
				&pRadialBrush
			);
			pRenderTarget->FillRectangle(ConvertToD2D1_RECT_F(rect), pRadialBrush);
			pRadialBrush->Release();
		}
		pRenderTarget->EndDraw();
	}
	void CRenderWin7OrPlus::DrawGradientRoundedRect(IWindow * pWnd, const MRect & rect, float xRadius, float yRadius, const MColor & firstColor, const MColor & secondColor, int lineStyle, bool bRadius)
	{
	}
	void CRenderWin7OrPlus::FillGradientRoundedRect(IWindow * pWnd, const MRect & rect, float xRadius, float yRadius, const MColor & firstColor, const MColor & secondColor, bool bRadius )
	{
	}
	void CRenderWin7OrPlus::DrawTextA(IWindow * pWnd, const AString & text, const MRect& rect,const MColor& color,int nSize,int Font)
	{
		DrawTextW(pWnd, text.ToStdWString().c_str(), rect, color, nSize,Font);
	}
	void CRenderWin7OrPlus::DrawTextW(IWindow * pWnd, const WString & text, const MRect& rect, const MColor& color, int nSize,int Font)
	{
		CWindowForWin7OrPlus* pWindow = static_cast<CWindowForWin7OrPlus*>(pWnd);
		ID2D1HwndRenderTarget* pRenderTarget = static_cast<ID2D1HwndRenderTarget*>(pWnd->GetNativeRenderTarget());
		auto pDWFactory = pWindow->m_pDWriteFactory;
		IDWriteTextFormat *pTextFormat;
		ID2D1SolidColorBrush *pBrush;
		DWRITE_TRIMMING trim;
		pDWFactory->CreateTextFormat(
			L"微软雅黑",
			NULL,
			DWRITE_FONT_WEIGHT_REGULAR,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			nSize,
			L"zh-cn",
			&pTextFormat
		);
		IDWriteInlineObject *pIDWriteInlineObject;
		pDWFactory->CreateEllipsisTrimmingSign(pTextFormat, &pIDWriteInlineObject);
		pTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
		pTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
		pTextFormat->SetWordWrapping(DWRITE_WORD_WRAPPING_WHOLE_WORD);
		trim.granularity = DWRITE_TRIMMING_GRANULARITY_CHARACTER;
		trim.delimiter = 1;
		trim.delimiterCount = 10;
		pTextFormat->SetTrimming(&trim, pIDWriteInlineObject);
		D2D1_RECT_F layoutRect = ConvertToD2D1_RECT_F(rect);
		pRenderTarget->CreateSolidColorBrush(
			ConvertToD2D1_COLOR_F(color),
			&pBrush
		);
		pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		pRenderTarget->BeginDraw();
		auto wide_string = text.ToStdWString();
		pRenderTarget->DrawTextA(
			wide_string.c_str(),
			wide_string.length(),
			pTextFormat,
			layoutRect,
			pBrush
		);
		pRenderTarget->EndDraw();
		pTextFormat->Release();
		pBrush->Release();
	}
	const MRect CRenderWin7OrPlus::CalculateTextA(IWindow * pWnd, const AString & text,int nSize, int Font)
	{
		return CalculateTextW(pWnd, text.ToStdWString().c_str(), nSize, Font);
	}
	const MRect CRenderWin7OrPlus::CalculateTextW(IWindow * pWnd, const WString & text,int nSize, int Font)
	{
		return CreateRect(0, nSize, 0, text.Length()*nSize);
	}
	MBitmap CRenderWin7OrPlus::GenerateBitmap(IWindow * pWnd, const MRect & targetRect)
	{
		return MBitmap();
	}
#endif // MDUILIB_WINVERSION_WIN7_OR_PLUS
}