
#ifndef MDUILIB_CORE_RENDERS_MRENDERS_H
#define MDUILIB_CORE_RENDERS_MRENDERS_H

#include"IRender.h"
namespace MDuilib
{
	class Render
	{
	public:
		//	@Remark:画或填充矩形。
		static void DrawRect(IWindow *pWnd, const MRect& rect, const MColor& color, int lineStyle) 
		{
			m_pCRender->DrawRect(pWnd, rect, color, lineStyle);
		};
		static void FillRect(IWindow *pWnd, const MRect& rect, const MColor& color) 
		{
			m_pCRender->FillRect(pWnd, rect, color);
		};
		//	@Remark:圆角矩形。
		static void DrawRoundedRect(IWindow *pWnd, const MRect& rect,const MColor &color,\
			float xRadius, float yRadius, int lineStyle) 
		{
			m_pCRender->DrawRoundedRect(pWnd, rect, color, xRadius, yRadius, lineStyle);
		};
		static void FillRoundedRect(IWindow *pWnd, const MRect& rect, const MColor &color, \
			float xRadius, float yRadius)
		{
			m_pCRender->FillRoundedRect(pWnd, rect, color, xRadius, yRadius);
		};
		//	@Remark:图片。
		static void DrawImage(IWindow* pWnd, const MBitmap &bitmap, const MRect &targetRect, \
			float fadeLevel = 1.0f, bool bUseAlpha = false) 
		{
			m_pCRender->DrawImage(pWnd, bitmap, targetRect, fadeLevel, bUseAlpha);
		};
		//	@Remark:带颜色梯度的矩形。
		static void DrawGradientRect(IWindow* pWnd, const MRect& rect, \
			const MColor& firstColor, const MColor &secondColor, \
			int lineStyle, bool bRadius = false
		) 
		{
			m_pCRender->DrawGradientRect(pWnd, rect, firstColor, secondColor, lineStyle, bRadius);
		};
		static void FillGradientRect(IWindow* pWnd, const MRect& rect, \
			const MColor& firstColor, const MColor &secondColor, \
			bool bRadius = false
		) 
		{
			m_pCRender->FillGradientRect(pWnd, rect, firstColor, secondColor, bRadius);
		};
		//	@Remark:带颜色梯度的圆角矩形。
		static void DrawGradientRoundedRect(IWindow* pWnd, const MRect& rect, \
			float xRadius, float yRadius, \
			const MColor& firstColor, const MColor &secondColor, \
			int lineStyle, bool bRadius = false
		) 
		{
			m_pCRender->DrawGradientRoundedRect(pWnd, rect, xRadius, yRadius, firstColor, secondColor, lineStyle, bRadius);
		};
		static void FillGradientRoundedRect(IWindow* pWnd, const MRect& rect, \
			float xRadius, float yRadius, \
			const MColor& firstColor, const MColor &secondColor, \
			bool bRadius = false
		) 
		{
			m_pCRender->FillGradientRoundedRect(pWnd, rect, xRadius, yRadius, firstColor, secondColor, bRadius);
		};
		//	@Remark:渲染文本。
		static void DrawTextA(IWindow *pWnd, const AString& text, const MRect& rect, const MColor &color,int nSize, int Font)
		{
			m_pCRender->DrawTextA(pWnd, text, rect, color,nSize,Font);
		};
		static void DrawTextW(IWindow *pWnd, const WString& text, const MRect& rect, const MColor &color,int nSize, int Font)
		{
			m_pCRender->DrawTextW(pWnd, text, rect, color,nSize,Font);
		};
		//	@Remark:计算渲染文本所需的矩形大小。
		static const MRect CalculateTextA(IWindow *pWnd, const AString& text, int nSize,int Font) 
		{
			return m_pCRender->CalculateTextA(pWnd, text, nSize,Font);
		};
		static const MRect CalculateTextW(IWindow *pWnd, const WString& text, int nSize,int Font) 
		{
			return m_pCRender->CalculateTextW(pWnd, text, nSize,Font);
		};
		//	@Remark:从窗体中获取对应位置的矩形位图。
		static MBitmap GenerateBitmap(IWindow *pWnd, const MRect &targetRect) 
		{
			m_pCRender->GenerateBitmap(pWnd, targetRect);
		};
		static IRender* m_pCRender;
	};
}

#include"../PlatformSpecific/Utils.h"
namespace MDuilib
{
#if MDUILIB_WINVERSION_WIN7_OR_PLUS
	class CRenderWin7OrPlus : public IRender
	{
	public:
		// Inherited via IRender
		virtual void DrawRect(IWindow * pWnd, const MRect & rect, const MColor & color, int lineStyle) override;
		virtual void FillRect(IWindow * pWnd, const MRect & rect, const MColor & color) override;
		virtual void DrawRoundedRect(IWindow * pWnd, const MRect & rect, const MColor& color, \
			float xRadius, float yRadius, int lineStyle) override;
		virtual void FillRoundedRect(IWindow * pWnd, const MRect & rect, const MColor& color, \
			float xRadius, float yRadius) override;
		virtual void DrawImage(IWindow * pWnd, const MBitmap & bitmap, const MRect & targetRect, float fadeLevel = 1.0f, bool bUseAlpha = false) override;
		virtual void DrawGradientRect(IWindow * pWnd, const MRect & rect, const MColor & firstColor, const MColor & secondColor, int lineStyle, bool bRadius = false) override;
		virtual void FillGradientRect(IWindow * pWnd, const MRect & rect, const MColor & firstColor, const MColor & secondColor, bool bRadius = false) override;
		virtual void DrawGradientRoundedRect(IWindow * pWnd, const MRect & rect, float xRadius, float yRadius, const MColor & firstColor, const MColor & secondColor, int lineStyle, bool bRadius = false) override;
		virtual void FillGradientRoundedRect(IWindow * pWnd, const MRect & rect, float xRadius, float yRadius, const MColor & firstColor, const MColor & secondColor, bool bRadius = false) override;
		virtual void DrawTextA(IWindow * pWnd, const AString & text, const MRect& rect, const MColor &color,int nSize, int Font) override;
		virtual void DrawTextW(IWindow * pWnd, const WString & text, const MRect& rect, const MColor &color, int nSize,int Font) override;
		virtual const MRect CalculateTextA(IWindow * pWnd, const AString & text, int nSize,int Font) override;
		virtual const MRect CalculateTextW(IWindow * pWnd, const WString & text, int nSize,int Font) override;
		virtual MBitmap GenerateBitmap(IWindow * pWnd, const MRect & targetRect) override;
	private:
	};
#endif // MDUILIB_WINVERSION_WIN7_OR_PLUS
}

#endif // !MDUILIB_CORE_RENDERS_MRENDERS_H
