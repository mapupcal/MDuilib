
#ifndef MDUILIB_CORE_ICONTROL_H
#define MDUILIB_CORE_ICONTROL_H

/*
*	@Remark:定义控件的基类类型。
*/
#include"../Utils/MString.h"
#include"../GeometriesDatas/MBitmap.h"
#include"../GeometriesDatas/MRect.h"
#include"Events.h"
namespace MDuilib
{
	class IControl;

	class IControl
	{
	public:
		IControl();
		virtual void Delete();
	protected:
		virtual ~IControl();
	public:
		virtual String GetName() const;
		virtual void SetName(const String &name);
		virtual String GetClass() const;
		virtual void* GetInterface(const String &interfaceName);
		virtual MUINT GetControlFlags() const;
		
		virtual bool Active();
		virtual IControl* GetParent() const;
		virtual IControl* GetCover() const;
		virtual void SetCover(IControl *pIControl);

		virtual String GetText() const;
		virtual String SetText() const;
		
		MColor GetBackColor() const;
		void SetBackColor(MColor color);
		MColor GetBackColor2() const;
		void SetBackColor2(MColor color);
		MColor GetBackColor3() const;
		void SetBackColor3(MColor color);
		String GetBackgroudImage() const;
		void SetBackgroupImage(String imageName) const;
		MColor GetFocusBorderColor() const;
		void SetFocusBorderColor(MColor color) const;

		MSize GetBorderRound() const;
		void SetBorderRound(MSize size);

		MColor GetBorderColor() const;
		void SetBorderColor(MColor color);
		MRect GetBorderRect() const;
		void SetBorderRect(MRect rect);
		void SerBorderSize(short iSize);
		int GetBorderStyle() const;
		void SetBorderStyle(int nStyle);

		virtual const MRect& GetPos() const;
		virtual MRect GetRelativePos() const;
		virtual MRect GetClientPos() const;
		virtual void SetPos(MRect rect, bool bNeedInvalidate = true);
		virtual void Move(MSize szOffset, bool bNeedInvalidate = true);
		virtual MSize::SizeType GetWidth() const;
		virtual MSize::SizeType GetHeight() const;
		virtual MSize::SizeType GetX() const;
		virtual MSize::SizeType GetY() const;
		virtual MRect GetPadding() const;
		virtual void SetPadding(MRect rectPadding);
		virtual MSize GetFixedXY() const;
		virtual void SetFixedXY(MSize szXY);

		virtual MSize::SizeType GetFixedWidth() const;
		virtual void SetFixedWidth(MSize::SizeType cx);
		virtual MSize::SizeType GetFixedHeight() const;
		virtual void SetFixedHeight(MSize::SizeType cy);
		virtual MSize::SizeType GetMinWidth() const;
		virtual void SetMinWidth(MSize::SizeType cx);
		virtual MSize::SizeType GetMaxWidth() const;
		virtual void SetMaxWidth(MSize::SizeType cx) const;
		virtual MSize::SizeType GetMinHeight() const;
		virtual void SetMinHeight(MSize::SizeType cy);
		virtual MSize::SizeType GetMaxHeight() const;
		virtual void SetMaxHeight(MSize::SizeType cy) const;

		virtual String GetToolTip() const;
		virtual void SetToolTip(const String &tips);
		virtual void SetToolTipWidth(int nWidth);
		virtual int GetToolTipWidth() const;

		virtual MCHAR GetShortcut() const;
		virtual void SetShortcut(MCHAR ch);

		virtual bool IsContextMenuUsed() const;
		virtual void SetContextMenuUsed(bool bMenuUsed);

		virtual bool IsVisible() const;
		virtual void SetVisible(bool bVisible = true);
		virtual bool IsEnabled() const;
		virtual void SetEnabled(bool bEnabled = true);
		virtual bool IsMouseEnabled() const;
		virtual void SetMouseEnabled(bool pEnabled = true);
		virtual bool IsKeyBoardEnabled() const;
		virtual void SetKeyBoardEnabled(bool pEnable = true);
		virtual bool IsFocused() const;
		virtual void SetFocus();
		virtual bool IsFloatable() const;
		virtual void SetFloatable(bool pFloatable = true);

		void Invalidate();
		bool IsUpdateNeeded() const;
		void NeedUpdate();
		void NeedParentUpdate();
		MColor GetAdjustColor(MColor dwColor);

		virtual void Init();
		virtual void DoInit();
	protected:
		/*
		*	@FuncName：Event
		*	@Remark:负责处理内部事件的数据类型。
		*/
		virtual void Event(MEvent& event);
		virtual void DoEvent(MEvent& event);

		/*
		*	@FunctionName:RaiseEvent
		*	@Parameter:rNotifyEvents<EventArgs>，注册了该事件的回调函数列表。
		*	@Parameter:rEventArgs,当某个事件触发时返回给回调函数的参数。
		*/
		template<typename EventArgs>
		void RaiseEvent(
			NotifyEvents<EventArgs> &rNotifyEvents, 
			EventArgs &rEventArgs)
		{
			for (const auto& handler : rNotifyEvents)
			{
				handler(this, rEventArgs);
			}
		}
	};
}
#endif // !MDUILIB_CORE_ICONTROL_H
