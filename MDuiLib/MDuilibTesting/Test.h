#ifndef MDUILIB_TESTING_TEST_H
#define MDUILIB_TESTING_TEST_H

#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<exception>
//	@MicroRemark:将宏参数转化成字符串
#define MDUILIB_PARAM_TO_STR(Param) (#Param)
#define MDUILIB_CATER_PARAM_STR(Param1,Param2) (Param1##Param2)

#define _MDUILIB_TEST

//#	TODO:为什么不能计算这两个变量？
static unsigned int gTestCaseNum(0);
static unsigned int gTestSuccessNum(0);

namespace MDuilibTest
{
/*
*	@TemplateFunctionName:EqualBase
*	@Remark:使用CompareFunctor比较expected和actual是否相同,利用FormatFunctor格式化输出。
*	@Noted:bool CompareFunctor(Type expected,Type actual);
*	@Noted:std::string FormatFunctor(Type expected);
*/
	template<typename Type,typename CompareFunctorType,typename FormatFunctorType>
	static bool EqualBase(Type expected, Type actual, \
		CompareFunctorType CompareFunctor, \
		FormatFunctorType FormatFunctor)
	{
		if (CompareFunctor(expected, actual))
		{
			//	Pass the Equal Test.
			return true;
		}
		else
		{
			//	Debug模式下检测FormatFunctorType是否满足约定条件
#ifdef _DEBUG
			auto formatString = FormatFunctor(actual);
#endif
			return false;
		}
	}

	//@Remark:CompareFunctor For TEST_ASSERT 
	static auto TestAssertCompareFunctor = [](bool expected, bool actual)\
		-> bool {return expected == actual; };
	//@Remark:FormatFunctor For TEST_ASSERT
	static auto TestAssertFormatFunctor = [](bool BoolVar)\
		->std::string{return BoolVar ? "true" : "false"; };

	//@Remark:CompareFunctor For EQUAL_SIZE
	static auto EqualSizeCompareFunctor = [](size_t expected, size_t actual)\
		->bool {return expected == actual; };
	//@Remark:FormatFucntor For EQUAL_SIZE
	static auto EqualSizeFormatFunctor = [](size_t sz)->std::string\
	{return std::to_string(sz); };

#define EQUAL_BASE(expected,actual,CompareFunctor,FormatFunctor)\
	do\
	{\
		::gTestCaseNum++;\
		if(!EqualBase(expected,actual,CompareFunctor,FormatFunctor))\
		{\
			auto ExpectedFormatString = FormatFunctor(expected);\
			auto ActualFormatString = FormatFunctor(actual);\
			printf(\
				"----------Failed Test----------\n" \
				"FilePath:%s\n" \
				"Line:%d\n" \
				"Expected:\n%s\n" \
				"Actual:\n%s\n" \
				"----------Failed End----------\n",\
				__FILE__,\
				__LINE__,\
				ExpectedFormatString.c_str(),\
				ActualFormatString.c_str()\
				);\
		}\
		else\
			::gTestSuccessNum++;\
	}\
	while(0)\

#define TEST_ASSERT(BoolExpresstion) \
	EQUAL_BASE(true,BoolExpresstion,TestAssertCompareFunctor,TestAssertFormatFunctor)



#ifdef _DEBUG
#define TEST_THROW_EXCEPTION(expression_block)\
	do\
	{\
		bool exception_flag = false;\
		try\
		{\
			expression_block;\
		}\
		catch (std::exception &)\
		{\
			exception_flag = true;	\
		}\
		TEST_ASSERT(exception_flag);\
	}while(0)\

#else
#define TEST_THROW_EXCEPTION(expression_block) /*Remove expression*/
#endif

#define EQUAL_SIZE(expected,actual) \
	EQUAL_BASE(size_t(expected),size_t(actual),EqualSizeCompareFunctor,EqualSizeFormatFunctor)

#define TEST_RESULT_STATISTICS()\
	do\
	{\
		double SuccessRate = (1.0f*gTestSuccessNum)/gTestCaseNum;\
		printf(\
				"\n-----------Test Statistics-----------\n"\
				"Total:%ld\n"\
				"Success:%ld\n"\
				"SuccessRate:%f\n"\
				"-----------Test Statistics----------\n",\
				gTestCaseNum,\
				gTestSuccessNum,\
				SuccessRate\
			); \
	}\
	while(0)\

}

#endif // !MDUILIB_TESTING_TEST_H
