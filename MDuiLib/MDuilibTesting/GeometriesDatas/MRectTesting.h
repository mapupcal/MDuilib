#pragma once
#include"../Test.h"
#include"../../MDuilib/GeometriesDatas/MRect.h"

/*
*	@Remark:定义MRectTesting所需要的相关宏
*/

namespace MDuilibTest
{
	static auto MRectCompareFunctor 
		= [](const MDuilib::MRect& expected, const MDuilib::MRect& actual)->bool\
	{
		return 
			(expected.top == actual.top) && \
			(expected.bottom == actual.bottom) && \
			(expected.left == actual.left) && \
			(expected.right == actual.right);
	};

	static auto MRectFormatFunctor 
		= [](const MDuilib::MRect& rect)->std::string
	{
		return "top:" + std::to_string(rect.top) + "\tbottom:" + std::to_string(rect.bottom)\
			+ "\tleft:" + std::to_string(rect.left) + "\tright:" + std::to_string(rect.right);
	};

//	@Remark:Equal Micro.
#define EQUAL_RECT(expected,actual)\
	EQUAL_BASE(expected,actual,MRectCompareFunctor,MRectFormatFunctor)

//	@Remark: Test whether the rect is valid or not.
#define VALID_RECT(rect)\
	TEST_ASSERT(MDuilib::IsValidRect(rect))

//	@Remark: See above.
#define INVALID_RECT(rect)\
	TEST_ASSERT(!MDuilib::IsValidRect(rect))

	void MRectTestingEntry();
}