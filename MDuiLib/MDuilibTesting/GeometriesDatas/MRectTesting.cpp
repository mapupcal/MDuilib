#include"MRectTesting.h"
#include<vector>
using namespace MDuilib;

namespace MDuilibTest
{
	void MRectTestingEntry()
	{
		{	//@TestRemark: Test IsValidRect
			MRect rect = { 0,100,0,100 };
			TEST_ASSERT(IsValidRect(rect));
			rect = { 0,1,200,300 };
			TEST_ASSERT(IsValidRect(rect));
			
			rect = { 0,0,0,0 };	//成点
			TEST_ASSERT(IsValidRect(rect));

			rect = { 0,0,0,100 };//水平线
			TEST_ASSERT(IsValidRect(rect));

			rect = { 0,100,0,0 };//垂直线
			TEST_ASSERT(IsValidRect(rect));

			rect = { 1,0,3,100 };
			TEST_ASSERT(!IsValidRect(rect));
			rect = { 0,100,100,0 };
			TEST_ASSERT(!IsValidRect(rect));
		}
		{	//	@TestRemark:Normal Test.
			MRect rect = CreateRect(0, 1080, 0, 1920);
			VALID_RECT(rect);
			rect = { 1080, 0, 0, 1920 };
			INVALID_RECT(rect);
			rect = { 1080, 0, 1920, 0 };
			INVALID_RECT(rect);
			rect = { 0, 1080, 1920, 0 };
			INVALID_RECT(rect);
		}
		{	//	@TestRemark:UnionRect Test
			//	@TestRemark: rect2 locates inside the rect1.
			MRect rect1 = CreateRect(40, 150, 40, 150);
			MRect rect2 = CreateRect(50, 100, 50, 100);
			MRect result = UnionRect(rect1, rect2);
			MRect expected = rect1;
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect1和rect2重合
			rect2 = rect1;
			result = UnionRect(rect1, rect2);
			VALID_RECT(result);
			EQUAL_RECT(rect1, result);

			//	@TestRemark: rect2 和 rect1 具备相同的宽度，但不相同的高度
			//				 即垂直方向具有重合的区域
			rect1 = CreateRect(40, 150, 40, 150);
			rect2 = CreateRect(30, 140, 40, 150);
			result = UnionRect(rect1, rect2);
			expected = { 30,150,40,150 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 和 rect1 具备相同的高度，但不相同的宽度
			rect1 = CreateRect(40, 150, 40, 150);
			rect2 = CreateRect(40, 150, 30, 140);
			result = UnionRect(rect1, rect2);
			expected = { 40,150,30,150 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 与 rect1 垂直成 T 字形
			rect1 = CreateRect(0, 100, 0, 100);
			rect2 = CreateRect(30, 120, 30, 120);
			result = UnionRect(rect1, rect2);
			expected = { 0,120,0,120 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 与 rect1 垂直成 + 字形
			rect1 = CreateRect(30, 80, 0, 100);
			rect2 = CreateRect(0, 100, 30, 80);
			result = UnionRect(rect1, rect2);
			expected = { 0,100,0,100 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark:rect1 与 rect2 在对角重合
			rect1 = CreateRect(0, 80, 0, 80);
			rect2 = CreateRect(30, 100, 30, 100);
			result = UnionRect(rect1, rect2);
			expected = { 0,100,0,100 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect1 和 rect2 完全没有重合部分
			rect1 = CreateRect(0, 100, 0, 100);
			rect2 = CreateRect(200, 300, 200, 300);
			result = UnionRect(rect1, rect2);
			expected = { 0,300,0,300 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);
		}
		{	//	@TestRemark: rect1 
			//	@TestRemark: rect2 locates inside the rect1.
			MRect rect1 = CreateRect(40, 150, 40, 150);
			MRect rect2 = CreateRect(50, 100, 50, 100);
			MRect result = IntersectRect(rect1, rect2);
			MRect expected = rect2;
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect1和rect2重合
			rect2 = rect1;
			result = IntersectRect(rect1, rect2);
			VALID_RECT(result);
			EQUAL_RECT(rect1, result);

			//	@TestRemark: rect2 和 rect1 具备相同的宽度，但不相同的高度
			//				 即垂直方向具有重合的区域
			rect1 = CreateRect(40, 150, 40, 150);
			rect2 = CreateRect(30, 140, 40, 150);
			result = IntersectRect(rect1, rect2);
			expected = { 40,140,40,150 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 和 rect1 具备相同的高度，但不相同的宽度
			rect1 = CreateRect(40, 150, 40, 150);
			rect2 = CreateRect(40, 150, 30, 140);
			result = IntersectRect(rect1, rect2);
			expected = { 40,150,40,140 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 与 rect1 垂直成 T 字形
			rect1 = CreateRect(0, 100, 0, 100);
			rect2 = CreateRect(30, 120, 30, 120);
			result = IntersectRect(rect1, rect2);
			expected = { 30,100,30,100 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect2 与 rect1 垂直成 + 字形
			rect1 = CreateRect(30, 80, 0, 100);
			rect2 = CreateRect(0, 100, 30, 80);
			result = IntersectRect(rect1, rect2);
			expected = { 30,80,30,80 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark:rect1 与 rect2 在对角重合
			rect1 = CreateRect(0, 80, 0, 80);
			rect2 = CreateRect(30, 100, 30, 100);
			result = IntersectRect(rect1, rect2);
			expected = { 30,80,30,80 };
			VALID_RECT(result);
			EQUAL_RECT(expected, result);

			//	@TestRemark: rect1 和 rect2 完全没有重合部分
			rect1 = CreateRect(0, 100, 0, 100);
			rect2 = CreateRect(200, 300, 200, 300);
			result = IntersectRect(rect1, rect2);
			INVALID_RECT(result);
		}

		{	//	@TestRemark: IsPointInRect Testing
			MPoint point = { 40,50 };
			MRect rect = CreateRect(30, 100, 30, 100);
			TEST_ASSERT(IsPointInRect(point, rect));
			
			point = { 30,30 };
			rect = CreateRect(30, 100, 30, 100);
			TEST_ASSERT(IsPointInRect(point, rect));

			point = { 100,100 };
			TEST_ASSERT(IsPointInRect(point, rect));

			point = { 30,100 };
			TEST_ASSERT(IsPointInRect(point, rect));

			point = { 100,30 };
			TEST_ASSERT(IsPointInRect(point, rect));

			point = { 50,101 };
			TEST_ASSERT(!IsPointInRect(point, rect));

			point = { 29,80 };
			TEST_ASSERT(!IsPointInRect(point, rect));
		}

		{	//	@TestRemark: RectHitTest Testing
			auto RectHitTestHelper = [](std::vector<MRect> rects) -> bool
			{
				return RectHitTest(rects[0], rects[1]);
			};
			//	@TestRemark: Normal Testing
			TEST_ASSERT(RectHitTestHelper(
			{ { 0,100,0,100 }, { 50,150,50,150 } }
			));
			TEST_ASSERT(RectHitTestHelper(
			{ { 50,150,50,150 }, { 0,100,0,100 } }
			));
			TEST_ASSERT(RectHitTestHelper(
			{ { 50,150,0,100 },{ 0,100,50,150 } }
			));
			TEST_ASSERT(RectHitTestHelper(
			{ { 0,100,50,150 },{ 50,150,0,100 } }
			));
			TEST_ASSERT(!RectHitTestHelper(
			{ { 0,100,0,100 },{ 101,150,101,150 } }
			));
			TEST_ASSERT(!RectHitTestHelper(
			{ { 0,100,0,100 },{ 500,1500,200,1000 } }
			));
			TEST_ASSERT(!RectHitTestHelper(
			{ { 0,0,0,0 },{ 1,1,1,1 } }
			));
			TEST_ASSERT(!RectHitTestHelper(
			{ { 0,150,0,80 } ,{ 160,1000,90,100 } }
			));
		}
	}
}