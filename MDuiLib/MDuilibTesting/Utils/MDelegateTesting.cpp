#include"MDelegateTesting.h"
#include"../Test.h"
#include<vector>
using namespace std;
using namespace MDuilib;
namespace MDuilibTest
{

	namespace DelegateTest
	{
		void DelegateFunction(int value)
		{
			printf("Normal Delgate Test. Value[%d]\n", value);
		}

		static void DelegateStaticFunction(int value)
		{
			printf("Static Delgate Test. Value[%d]\n", value);
		}
	}

	class __DelegateTestClassA
	{
	public:
		void DelegateFunction(int value) 
		{
			printf("Normal Delgate Test.Class[A] Value[%d]\n", value);
		}
		static void ClassStaticDelegateFunction(int value)
		{
			printf("Static Delgate Test.Class[A] Value[%d]\n", value);
		}
		virtual void DelegateVirtualFunction(int value) 
		{
			printf("Virtual Delgate Test.Class[A] Value[%d]\n", value);
		}
		virtual ~__DelegateTestClassA()
		{

		}
	};

	class __DelegateTestClassB : public __DelegateTestClassA
	{
	public:
		void DelegateFunction(int value)
		{
			printf("Normal Delgate Test.Class[B] Value[%d]\n", value);
		}
		virtual void DelegateVirtualFunction(int value) override
		{
			printf("Virtual Delgate Test.Class[B] Value[%d]\n", value);
		}
	};

	class __DelegateTestClassC :public __DelegateTestClassB
	{
	public:
		void DelegateFunction(int value)
		{
			printf("Normal Delgate Test.Class[C] Value[%d]\n", value);
		}
		virtual void DelegateVirtualFunction(int value) override
		{
			printf("Virtual Delgate Test.Class[C] Value[%d]\n", value);
		}
	};

	void MDelegateTestingEntry()
	{
		//	@TestCaseRemark:Test Case 
		{	
			Delegate<void(int)> d = DelegateTest::DelegateFunction;//d(1);
			d = DelegateTest::DelegateStaticFunction;//d(2);
			d = MakeDelegate(std::make_shared<__DelegateTestClassA>(), &__DelegateTestClassA::DelegateFunction);//d(3);
			d = MakeDelegate(std::make_shared<__DelegateTestClassB>(), &__DelegateTestClassA::DelegateFunction);//d(4);
			d = MakeDelegate(std::make_shared<__DelegateTestClassB>(), &__DelegateTestClassB::DelegateFunction);//d(5);
			d = MakeDelegate(std::make_shared<__DelegateTestClassA>(), &__DelegateTestClassA::DelegateVirtualFunction);//d(6);
			d = MakeDelegate(std::make_shared<__DelegateTestClassB>(), &__DelegateTestClassA::DelegateVirtualFunction);//d(7);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassA::DelegateFunction);//d(8);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassB::DelegateFunction);//d(9);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassC::DelegateFunction);//d(10);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassA::DelegateVirtualFunction);//d(8);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassB::DelegateVirtualFunction);//d(9);
			d = MakeDelegate(std::make_shared<__DelegateTestClassC>(), &__DelegateTestClassC::DelegateVirtualFunction);//d(10);
		}
	}
}