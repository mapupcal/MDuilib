#include"MStringTesting.h"
using namespace MDuilib;

namespace MDuilibTest
{
	void MStringTestingEntry()
	{
		{	//	@TestRemark:AString Constructor Test
			AString null_str = AString();
			EQUAL_ASTRING(AString(), null_str);
			TEST_ASSERT(null_str.IsNull());
			EQUAL_SIZE(1, null_str.RefCount());
			EQUAL_SIZE(0, null_str.Length());
			auto null_str1 = null_str;
			TEST_ASSERT(null_str1.IsNull());
			EQUAL_SIZE(2, null_str1.RefCount());
			EQUAL_SIZE(0, null_str1.Length());
			null_str = "";
			TEST_ASSERT(null_str1.IsNull());
			EQUAL_SIZE(1, null_str1.RefCount());
			EQUAL_SIZE(0, null_str1.Length());

			EQUAL_ASTRING(AString(), null_str);
			TEST_ASSERT(null_str.IsNull());
			TEST_ASSERT(null_str.RefCount() == 1);
			TEST_ASSERT(null_str.Length() == 0);

			AString str("abcdefg");
			EQUAL_ASTRING(AString("abcdefg"), str);
			EQUAL_SIZE(str.Length(), 7);
			char* c_str= "abcdefg";
			AString str1 = c_str;
			EQUAL_ASTRING(AString("abcdefg"), str1);
			EQUAL_SIZE(str1.RefCount(), 1);
			
			//	@TestRemark:Copy Constructor
			AString str2(str1);
			EQUAL_SIZE(str2.RefCount(), 2);
			str2 = str;
			EQUAL_SIZE(1, str1.RefCount());
			EQUAL_SIZE(2, str2.RefCount());
			EQUAL_SIZE(2, str.RefCount());
			EQUAL_SIZE(str.Length(), str2.Length());
			EQUAL_ASTRING(str, str2);

			//	@TestRemark:Move Constructor
			AString str3 = std::move(str);
			EQUAL_SIZE(2, str3.RefCount());
			EQUAL_SIZE(2, str2.RefCount());
			EQUAL_ASTRING(str2, str3);
			//TEST_ASSERT(str.Buffer() == nullptr);

			AString str_ch('1', 1);
			EQUAL_ASTRING(AString("1"), str_ch);
			str_ch = AString('1', 10);
			EQUAL_ASTRING(AString("1111111111"), str_ch);
			str_ch = std::move(AString('1', 5));
			EQUAL_ASTRING(AString("11111"), str_ch);
		}
		{	//	@TestRemark:测试引用计数
			AString str6 = "123456";
			AString str6_1 = str6;
			EQUAL_SIZE(2, str6.RefCount());
			AString str6_2 = str6_1;
			EQUAL_SIZE(3, str6.RefCount());
			AString str5 = "12345";
			AString str5_1 = str5;
			str5_1 = str6;
			EQUAL_SIZE(1, str5.RefCount());
			EQUAL_SIZE(4, str5_1.RefCount());
			EQUAL_SIZE(4, str6.RefCount());
			str5 = str6;
			//TODO:这个时候“12345”串的内存被释放，如何测试?
			EQUAL_SIZE(5, str6.RefCount());
			EQUAL_SIZE(5, str6.RefCount());
			//	@TestRemark:测试自赋值
			str6 = str6;
			EQUAL_SIZE(5, str6.RefCount());
			//	@TestRemark:指向相同buffer指针的字符串赋值
			str6 = str6_1;
			EQUAL_SIZE(5, str6.RefCount());
			str6 = str6_1 = str6_2 = str5 = str5_1;
			EQUAL_SIZE(5, str6.RefCount());
			EQUAL_SIZE(5, str6_1.RefCount());
			EQUAL_SIZE(5, str6_2.RefCount());
			EQUAL_SIZE(5, str5.RefCount());
			EQUAL_SIZE(5, str5_1.RefCount());

			str5 = str5_1 = "12345";
			EQUAL_SIZE(3, str6.RefCount());
			EQUAL_SIZE(3, str6_1.RefCount());
			EQUAL_SIZE(3, str6_2.RefCount());
			EQUAL_SIZE(2, str5.RefCount());
			EQUAL_SIZE(2, str5_1.RefCount());
		}
		{	//	@TestRemark:Append
			AString str;
			str.Append("1").Append("2").Append("3");
			EQUAL_ASTRING(AString("123"), str);
			str.Append("");
			EQUAL_ASTRING(AString("123"), str);
			str.Append("456");
			EQUAL_ASTRING(AString("123456"), str);
			str = str.Left(3);
			EQUAL_ASTRING(AString("123"), str);
			EQUAL_SIZE(1, str.RefCount());
			str = "123456";
			EQUAL_SIZE(1, str.RefCount());
			str = str.Right(3);
			EQUAL_ASTRING(AString("456"), str);
			const void* pbuffer = reinterpret_cast<const void*>(str.Buffer());
			str.Append('1').Append('2').Append('3');
			TEST_ASSERT(pbuffer == str.Buffer());
			EQUAL_ASTRING(AString("456123"), str);

			str = "123";
			pbuffer = reinterpret_cast<const void*>(str.Buffer());
			auto str1 = str;
			str1.Append("456");
			TEST_ASSERT(pbuffer == str.Buffer());
			TEST_ASSERT(pbuffer != str1.Buffer());
			EQUAL_SIZE(1, str.RefCount());
			EQUAL_SIZE(1, str1.RefCount());
			EQUAL_ASTRING(AString("123456"), str1);
			EQUAL_ASTRING(AString("123"), str);
			str.Append('a', 10);
			EQUAL_ASTRING(AString("123aaaaaaaaaa"), str);
			str = str.Left(3);
			EQUAL_ASTRING(AString("123"), str);
			pbuffer = reinterpret_cast<const void*>(str.Buffer());
			str.Append('a', 10);
			TEST_ASSERT(pbuffer == str.Buffer());
		}
		{	//	@TestRemark:Prepend
			AString str;
			str.Prepend("1").Prepend("2").Prepend("3");
			EQUAL_ASTRING(AString("321"), str);
			str.Prepend("");
			EQUAL_ASTRING(AString("321"), str);
			str.Prepend("456");
			EQUAL_ASTRING(AString("456321"), str);
			str = str.Left(3);
			EQUAL_ASTRING(AString("456"), str);
			EQUAL_SIZE(1, str.RefCount());
			str = "123456";
			EQUAL_SIZE(1, str.RefCount());
			str = str.Right(3);
			EQUAL_ASTRING(AString("456"), str);
			const void* pbuffer = reinterpret_cast<const void*>(str.Buffer());
			str.Prepend('1').Prepend('2').Prepend('3');
			TEST_ASSERT(pbuffer == str.Buffer());
			EQUAL_ASTRING(AString("321456"), str);

			str = "123";
			pbuffer = reinterpret_cast<const void*>(str.Buffer());
			auto str1 = str;
			str1.Prepend("456");
			TEST_ASSERT(pbuffer == str.Buffer());
			TEST_ASSERT(pbuffer != str1.Buffer());
			EQUAL_SIZE(1, str.RefCount());
			EQUAL_SIZE(1, str1.RefCount());
			EQUAL_ASTRING(AString("456123"), str1);
			EQUAL_ASTRING(AString("123"), str);
			str.Prepend('a', 10);
			EQUAL_ASTRING(AString("aaaaaaaaaa123"), str);
			str = str.Left(3);
			EQUAL_ASTRING(AString("aaa"), str);
			pbuffer = reinterpret_cast<const void*>(str.Buffer());
			str.Prepend('a', 10);
			TEST_ASSERT(pbuffer == str.Buffer());
		}

		//	@TestRemark:Insert
		{
			AString str = "123456";
			str.Insert(3, "789");
			EQUAL_ASTRING(AString("123789456"), str);
			str.Insert(3, "789");
			EQUAL_ASTRING(AString("123789789456"), str);
			str = str.Left(6);
			const void* pbuffer = str.Buffer();
			str.Insert(3, "456");
			TEST_ASSERT(str.Buffer() == pbuffer);
			EQUAL_ASTRING(AString("123456789"), str);
			TEST_THROW_EXCEPTION(str.Insert(-2, "456"));
			TEST_THROW_EXCEPTION(str.Insert(str.Length() + 1, "456"));

			AString str1 = str;
			str1.Insert(1, "");
			EQUAL_ASTRING(str, str1);
			EQUAL_SIZE(2, str.RefCount());
			TEST_ASSERT(str.Buffer() == str1.Buffer());

			str1.Insert(1, "00");
			EQUAL_ASTRING(AString("10023456789"), str1);
			TEST_ASSERT(str.Buffer() != str1.Buffer());
			EQUAL_SIZE(1, str.RefCount());
			EQUAL_SIZE(1, str1.RefCount());

			str = "aaa";
			str.Insert(1, 'b', 3);
			EQUAL_ASTRING(AString("abbbaa"), str);
			str.Insert(1, 'c');
			EQUAL_ASTRING(AString("acbbbaa"), str);
			TEST_THROW_EXCEPTION(str.Insert(-1, 'a'));
			TEST_THROW_EXCEPTION(str.Insert(str.Length() + 1, 'a'));

		}
		//@	TestRemark:Repeated
		{
			AString str = "10";
			AString str_rep2 = str.Repeated(2);
			EQUAL_ASTRING(AString("1010"), str_rep2);
			EQUAL_SIZE(1,str.RefCount());
			EQUAL_SIZE(1, str_rep2.RefCount());
			str = str.Repeated(0);
			str_rep2 = str.Repeated(2);
			EQUAL_ASTRING(AString(""), str_rep2);
			
			str = "MaA";
			str = str.Repeated(10);
			EQUAL_ASTRING(AString("MaAMaAMaAMaAMaAMaAMaAMaAMaAMaA"), str);
			str_rep2 = str.Left(1);
			EQUAL_SIZE(2, str_rep2.RefCount());
			EQUAL_SIZE(2, str.RefCount());
			str_rep2 = str_rep2.Repeated(2);
			EQUAL_SIZE(1, str.RefCount());
			EQUAL_SIZE(1, str_rep2.RefCount());
		}
		//	@TestRemark:子字符串操作 Left\Right\Mid
		{
			AString str = "123456789";
			auto pbuffer = str.Buffer();
			str = str.Left(9);
			EQUAL_ASTRING(AString("123456789"), str);
			EQUAL_SIZE(1, str.RefCount());
			TEST_ASSERT(str.Buffer() == pbuffer);
			auto mid = str.Mid(3, 3);
			auto left = str.Left(3);
			auto right = str.Right(3);
			EQUAL_ASTRING(AString("123"), left);
			EQUAL_ASTRING(AString("456"), mid);
			EQUAL_ASTRING(AString("789"), right);
			EQUAL_SIZE(3, left.Length());
			EQUAL_SIZE(3, mid.Length());
			EQUAL_SIZE(3, right.Length());
			EQUAL_SIZE(4, str.RefCount());
			EQUAL_SIZE(4, left.RefCount());
			EQUAL_SIZE(4, mid.RefCount());
			EQUAL_SIZE(4, right.RefCount());
			TEST_ASSERT(mid.Buffer() == str.Buffer());
			TEST_ASSERT(left.Buffer() == str.Buffer());
			TEST_ASSERT(right.Buffer() == str.Buffer());
			TEST_THROW_EXCEPTION(str.Left(10));
			TEST_THROW_EXCEPTION(str.Right(10));
			TEST_THROW_EXCEPTION(str.Mid(1, 9));
			TEST_THROW_EXCEPTION(str.Mid(10, 1));
			TEST_THROW_EXCEPTION(str.Mid(10, 0));
			mid = left = right;
			EQUAL_SIZE(4, str.RefCount());
			EQUAL_SIZE(4, left.RefCount());
			EQUAL_SIZE(4, mid.RefCount());
			EQUAL_SIZE(4, right.RefCount());
			TEST_ASSERT(mid.Buffer() == str.Buffer());
			TEST_ASSERT(left.Buffer() == str.Buffer());
			TEST_ASSERT(right.Buffer() == str.Buffer());
		}
		//	@TestRemark:CharAt
		{
			AString str = "0123456789";
			auto pbuffer = str.Buffer();
			TEST_THROW_EXCEPTION(str.CharAt(-1));
			TEST_THROW_EXCEPTION(str.CharAt(10));
			TEST_ASSERT('0' == str.CharAt(0));
			TEST_ASSERT('5' == str.CharAt(5));
			TEST_ASSERT('9' == str.CharAt(9));
			str = str.Right(9);//"123456789"
			TEST_ASSERT(pbuffer == str.Buffer());
			TEST_ASSERT('1' == str.CharAt(0));
			TEST_ASSERT('6' == str.CharAt(5));
			TEST_ASSERT('9' == str.CharAt(8));
			TEST_THROW_EXCEPTION(str.CharAt(-1));
			TEST_THROW_EXCEPTION(str.CharAt(9));

			str = "";
			TEST_THROW_EXCEPTION(str.CharAt(0));
		}
		//	@TestRemark:Upper\Lower
		{
			AString str = "123456aBCdEfG";
			auto up_str = str.ToUpper();
			auto low_str = str.ToLower();
			EQUAL_ASTRING(AString("123456ABCDEFG"), up_str);
			EQUAL_ASTRING(AString("123456abcdefg"), low_str);
			AString null_str;
			up_str = null_str.ToUpper();
			low_str = null_str.ToLower();
			EQUAL_ASTRING(AString(), up_str);
			EQUAL_ASTRING(AString(), low_str);
		}
		//	TestRemark:整数转化成字符串
		{
			//十进制
			AString int_str = AString::Number(-10);
			AString uint_str = AString::Number(int_str.Length());
			EQUAL_ASTRING(AString("-10"), int_str);
			EQUAL_ASTRING(AString("3"), uint_str);
			int_str = AString::Number(0);
			uint_str = AString::Number(0u);
			EQUAL_ASTRING(AString("0"), int_str);
			EQUAL_ASTRING(AString("0"), uint_str);
			int_str = AString::Number(10);
			uint_str = AString::Number(100u);
			EQUAL_ASTRING(AString("10"), int_str);
			EQUAL_ASTRING(AString("100"), uint_str);
			int_str = AString::Number(123);
			uint_str = AString::Number(1123u);
			EQUAL_ASTRING(AString("123"), int_str);
			EQUAL_ASTRING(AString("1123"), uint_str);

			//二进制
			int_str = AString::Number(-1,IntegerBase::BINARY);
			uint_str = AString::Number(1u,IntegerBase::BINARY);
			EQUAL_ASTRING(AString("-1"), int_str);
			EQUAL_ASTRING(AString("1"), uint_str);
			int_str = AString::Number(0, IntegerBase::BINARY);
			uint_str = AString::Number(0u, IntegerBase::BINARY);
			EQUAL_ASTRING(AString("0"), int_str);
			EQUAL_ASTRING(AString("0"), uint_str);
			int_str = AString::Number(3, IntegerBase::BINARY);
			uint_str = AString::Number(16u, IntegerBase::BINARY);
			EQUAL_ASTRING(AString("11"), int_str);
			EQUAL_ASTRING(AString("10000"), uint_str);
			
			//十六进制
			int_str = AString::Number(-1, IntegerBase::HEXADECIMAL);
			uint_str = AString::Number(1u, IntegerBase::HEXADECIMAL);
			EQUAL_ASTRING(AString("-0x1"), int_str);
			EQUAL_ASTRING(AString("0x1"), uint_str);
			int_str = AString::Number(0, IntegerBase::HEXADECIMAL);
			uint_str = AString::Number(0u, IntegerBase::HEXADECIMAL);
			EQUAL_ASTRING(AString("0x0"), int_str);
			EQUAL_ASTRING(AString("0x0"), uint_str);
			int_str = AString::Number(3, IntegerBase::HEXADECIMAL);
			uint_str = AString::Number(16u, IntegerBase::HEXADECIMAL);
			EQUAL_ASTRING(AString("0x3"), int_str);
			EQUAL_ASTRING(AString("0x10"), uint_str);
			int_str = AString::Number(31, IntegerBase::HEXADECIMAL);
			uint_str = AString::Number(65u, IntegerBase::HEXADECIMAL);
			EQUAL_ASTRING(AString("0x1f"), int_str);
			EQUAL_ASTRING(AString("0x41"), uint_str);

			//八进制
			int_str = AString::Number(-1, IntegerBase::OCTAL);
			uint_str = AString::Number(1u, IntegerBase::OCTAL);
			EQUAL_ASTRING(AString("-01"), int_str);
			EQUAL_ASTRING(AString("01"), uint_str);
			int_str = AString::Number(0, IntegerBase::OCTAL);
			uint_str = AString::Number(0u, IntegerBase::OCTAL);
			EQUAL_ASTRING(AString("00"), int_str);
			EQUAL_ASTRING(AString("00"), uint_str);
			int_str = AString::Number(3, IntegerBase::OCTAL);
			uint_str = AString::Number(16u, IntegerBase::OCTAL);
			EQUAL_ASTRING(AString("03"), int_str);
			EQUAL_ASTRING(AString("020"), uint_str);
			int_str = AString::Number(31, IntegerBase::OCTAL);
			uint_str = AString::Number(65u, IntegerBase::OCTAL);
			EQUAL_ASTRING(AString("037"), int_str);
			EQUAL_ASTRING(AString("0101"), uint_str);
		}
		//	@TestRemark:将浮点数转换为字符串,可能会导致转换不精确
		{
			EQUAL_ASTRING(AString("0.000000"), AString::Number(0.0f));
			EQUAL_ASTRING(AString("1.000001"), AString::Number(1.000001f));
			AString under_zero = AString::Number(0.141f);
			EQUAL_ASTRING(AString("0.141000"), under_zero);
			AString beyond_zero = AString::Number(1.141f);
			EQUAL_ASTRING(AString("1.141000"), beyond_zero);
			under_zero = AString::Number(0.00141f);
			EQUAL_ASTRING(AString("0.001410"), under_zero);
			beyond_zero = AString::Number(1.001411f);
			EQUAL_ASTRING(AString("1.001411"), beyond_zero);
			under_zero = AString::Number(0.456f);
			beyond_zero = AString::Number(123.456f);
			EQUAL_ASTRING(AString("0.456000"), under_zero);
			EQUAL_ASTRING(AString("123.456000"), beyond_zero);
		}
		//	@TestRemark:测试各种operator+
		{
			AString result = "123" + AString::Number(456);
			EQUAL_ASTRING(AString("123456"), result);
			result = 123 + AString("456");
			EQUAL_ASTRING(AString("123456"), result);
			result = AString("Real:") + 123.456f;
			EQUAL_ASTRING(AString("Real:123.456000"), result);
			result = 123.456f + AString("789");
			EQUAL_ASTRING(AString("123.456000789"), result);
		}
		//	@TestRemark:两字符串的比较、以及IsStartsWith、IsEndsWith等
		{
			AString num_str1 = "123456789";
			AString num_str2 = "123456789";
			TEST_ASSERT(num_str1 == num_str2);
			num_str2 = "";
			TEST_ASSERT(num_str1 > num_str2);
			num_str2 = "123";
			TEST_ASSERT(num_str1 > num_str2);
			num_str2 = "23";
			TEST_ASSERT(num_str1 < num_str2);
			AString alphabet_str = "abcdefg";
			AString alphabet_str1 = "AbcDefg";
			TEST_ASSERT((alphabet_str.Compare(alphabet_str1, CaseSensitivity::CaseInSensitive) == 0));

			AString str = "ABC.0.0.0.0D123Abcd";
			AString str1 = "abc.0.0.0.0d123ABCD";
			TEST_ASSERT((str.Compare(str1, CaseSensitivity::CaseInSensitive) == 0));

			str = "Abcd";
			str1 = "ABC";
			TEST_ASSERT(str.Compare(str1,CaseSensitivity::CaseInSensitive) > 0);

			TEST_ASSERT(str.IsStartsWith("AB", CaseSensitivity::CaseInSensitive));
			TEST_ASSERT(str.IsEndsWith("BcD", CaseSensitivity::CaseInSensitive));
			TEST_ASSERT(!str.IsEndsWith("BCD"));
			TEST_ASSERT(str.IsStartsWith("Abc"));
			TEST_ASSERT(str.IsEndsWith("d"));
			TEST_ASSERT(str.IsStartsWith('A'));
			TEST_ASSERT(!str.IsStartsWith('a'));
			TEST_ASSERT(str.IsEndsWith('d'));
			TEST_ASSERT(!str.IsEndsWith('D'));
		}
		//	@TestRemark:测试IndexOf,IsContains
		{
			AString str = "1234567890mapupcal1234567890";
			TEST_ASSERT(-1 == str.IndexOf("jasmine"));
			TEST_ASSERT(!str.IsContains("jasmine"));
			TEST_ASSERT(10 == str.IndexOf("mapupcal"));
			TEST_ASSERT(str.IsContains("mapupcal"));
			TEST_ASSERT(8 == str.IndexOf('9'));
			TEST_ASSERT(str.IsContains("9"));
			TEST_ASSERT(0 == str.IndexOf("1"));
			TEST_ASSERT(str.IsContains("1"));
			TEST_ASSERT(-1 == str.IndexOf("1234567890mapupcal1234567890123"));
			TEST_ASSERT(!str.IsContains("1234567890mapupcal1234567890123"));
			TEST_ASSERT(0 == str.IndexOf("1234567890mapupcal1234567890"));
			TEST_ASSERT(str.IsContains("1234567890mapupcal1234567890"));
			str = "";
			TEST_ASSERT(-1 == str.IndexOf("jasmine"));
			TEST_ASSERT(!str.IsContains("jasmine"));
			TEST_ASSERT(-1 == str.IndexOf("mapupcal"));
			TEST_ASSERT(-1 == str.IndexOf('9'));
			TEST_ASSERT(-1== str.IndexOf("1"));
			TEST_THROW_EXCEPTION(str.IndexOf(""));
			str = "1233211234567";
			TEST_ASSERT(2 == str.IndexOf("33"));
			TEST_ASSERT(2 == str.IndexOf("33211234567"));
			TEST_ASSERT(12 == str.IndexOf('7'));

			str = "123456789123";
			TEST_ASSERT(1 == str.IndexOf("2", 1));
			TEST_ASSERT(2 == str.IndexOf("3", 1));
			TEST_ASSERT(10 == str.IndexOf("2", 2));
			TEST_ASSERT(9 == str.IndexOf("12", 1));
			TEST_ASSERT(4 == str.IndexOf("5", 2));
			TEST_ASSERT(4 == str.IndexOf("5", 4));
			TEST_ASSERT(-1 == str.IndexOf("5", 5));
		}
		//	TestRemark:Count
		{
			AString str = "123456789123456789";
			TEST_ASSERT(2 == str.Count("1"));
			TEST_ASSERT(2 == str.Count("2"));
			TEST_ASSERT(2 == str.Count("9"));
			TEST_ASSERT(2 == str.Count("123456789"));
			TEST_ASSERT(0 == str.Count("0"));
			TEST_ASSERT(0 == str.Count("012"));
			TEST_ASSERT(2 == str.Count("789"));
			TEST_ASSERT(1 == str.Count("912"));
			str = "";
			TEST_ASSERT(0 == str.Count("1"));
			TEST_ASSERT(0 == str.Count("2"));
			TEST_ASSERT(0 == str.Count("9"));
			TEST_ASSERT(0 == str.Count("123456789"));
			TEST_ASSERT(0 == str.Count("0"));
			TEST_ASSERT(0 == str.Count("012"));
			TEST_ASSERT(0 == str.Count("789"));
			TEST_ASSERT(0 == str.Count("912"));
		}
		//	@TestRemark:Splict
		{
			AString str = "Author:Mapupcal";
			AString::StringList list = str.Splict(":");
			EQUAL_SIZE(2, list.size());
			EQUAL_ASTRING(AString("Author"), list[0]);
			EQUAL_ASTRING(AString("Mapupcal"), list[1]);
			list = str.Splict("Fuck");
			EQUAL_ASTRING(str, list[0]);
			list = str.Splict("a");
			EQUAL_SIZE(3, list.size());
			EQUAL_ASTRING(str.Left(8), list[0]);
			EQUAL_ASTRING(AString("pupc"), list[1]);
			EQUAL_ASTRING(AString("l"), list[2]);
			list = str.Splict("A", CaseSensitivity::CaseInSensitive);
			EQUAL_SIZE(3, list.size());
			EQUAL_ASTRING(AString("uthor:M"), list[0]);
			EQUAL_ASTRING(AString("pupc"), list[1]);
			EQUAL_ASTRING(AString("l"), list[2]);
			list = str.Splict("Author:Mapupcal");
			EQUAL_SIZE(0, list.size());
			list = str.Splict("Author");
			EQUAL_ASTRING(str.Right(9), list[0]);
			list = str.Splict("Mapupcal");
			EQUAL_SIZE(1, list.size());
			EQUAL_ASTRING(str.Left(7), list[0]);
			EQUAL_SIZE(1, list.size());
			str = "";
			list = str.Splict("Mapupcal");
			EQUAL_SIZE(0, list.size());
			str = "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20";
			list = str.Splict(" ");
			EQUAL_SIZE(20, list.size());
			EQUAL_ASTRING(AString("1"), list[0]);
			EQUAL_ASTRING(AString("20"), list[19]);
			EQUAL_ASTRING(AString("10"), list[9]);
		}
		//	@TestRemark:Replace
		{
			AString str = "1 2 3 4 5 6 7 8 9 10";
			str.Replace(" ", "+");
			EQUAL_ASTRING(AString("1+2+3+4+5+6+7+8+9+10"), str);
			str = "          ";
			str.Replace(" ", "+");
			EQUAL_ASTRING(AString("++++++++++"), str);
			str = "1          ";
			str.Replace("1", "+");
			EQUAL_ASTRING(AString("+          "), str);
			str = "123456789101112131415161718192021";
			str.Replace("1", "+");
			EQUAL_ASTRING(AString("+23456789+0+++2+3+4+5+6+7+8+9202+"), str);
			str = "";
			str.Replace("mapupcal", "mm");
			EQUAL_ASTRING(AString(""), str);
			str = "1";
			str.Replace("1", "");
			EQUAL_ASTRING(AString(""), str);
			str = "1";
			str = str.Repeated(10);
			str.Replace("1", "2");
			EQUAL_ASTRING(AString("2").Repeated(10), str);
			str = "mapupcal@hotmail.com";
			str.Replace("a", "A");
			EQUAL_ASTRING(AString("mApupcAl@hotmAil.com"), str);
			str = "abcdefgabcdefg";
			str.Replace("a", "A").Replace("b", "B").Replace("c", "C");
			EQUAL_ASTRING(AString("ABCdefgABCdefg"), str);
		}
	}
}