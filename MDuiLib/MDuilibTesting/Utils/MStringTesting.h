#pragma once
#include "../Test.h"
#include "../../MDuilib/Utils/MString.h"

namespace MDuilibTest
{
	static auto AStringCompareFunctor = [](
		const MDuilib::AString &expected, \
		const MDuilib::AString &actual) ->bool
	{
		auto e_beg = expected.cbegin();
		auto e_en = expected.cend();
		auto a_beg = actual.cbegin();
		auto a_en = actual.cend();
		while ((e_beg != e_en && a_beg != a_en) && \
			(*e_beg == *a_beg))
		{
			e_beg++; a_beg++;
		}

		if (e_beg != e_en || a_beg != a_en)
			return false;
		return true;
	};

	static auto AStringFormatFunctor = [](
		const MDuilib::AString &str)
		->std::string
	{
		return std::string(str.cbegin(), str.cend());
	};

#define EQUAL_ASTRING(expected,actual)\
	EQUAL_BASE(expected,actual,AStringCompareFunctor,AStringFormatFunctor)

	void MStringTestingEntry();
}