#include "Test.h"
#include"GeometriesDatas\MRectTesting.h"
#include"..\MDuilibTesting\Utils\MStringTesting.h"
#include"..\MDuilibTesting\Utils\MDelegateTesting.h"
#include"../MDuilib/Core/PlatformSpecific/MWindows.h"
#include"../MDuilib/Core/Renders/MRenders.h"
using namespace MDuilibTest;
using namespace MDuilib;


int main()
{
	//MRectTestingEntry();
	//MStringTestingEntry();
	//MDelegateTestingEntry();
	//TEST_RESULT_STATISTICS();
	MRect windowRect = CreateRect(100, 580, 100, 740);
	IWindow* window = new CWindowForWin7OrPlus();
	window->InitWindow("测试窗口", windowRect);
	MRect ori = CreateRect(0, 20, 0, 20);
	WString str;
	window->OnMessage += [&](IWindow *pWindow, MEvent &e)
	{
		switch (e.Type)
		{
		case MEventType::EVENT_CHAR:
		{
			str.Append(e.chKey);
			ori = Render::CalculateTextW(pWindow, str, 20, 0);
			Render::DrawTextW(pWindow, str, ori, { 255,0,0,255 }, 20, 0);
			Render::DrawRect(pWindow, ori, { 0,255,125,125 }, 0);
		}
		break;
		case MEventType::EVENT_MOUSE_MOVE:
		{

		}
		break;
		case MEventType::EVENT_LBUTTON_DOWN:
		{
			MRect rc = CreateRect(e.ptMouse.y, e.ptMouse.y + 100, e.ptMouse.x, e.ptMouse.x + 100);
			Render::DrawTextA(pWindow, u8"张欣照Mapupcal Chueng", rc, { 255,0,255,100 }, 10,0);
		}
		break;
		case MEventType::EVENT_RBUTTON_DOWN:
		{
			MRect rc = CreateRect(e.ptMouse.y, e.ptMouse.y + 100, e.ptMouse.x, e.ptMouse.x + 100);
			Render::DrawGradientRect(pWindow, rc, { 0,255,255,255 }, { 255,0,255,255 }, 0, true);
		}
		break;
		case MEventType::EVENT_MOUSE_HOVER:
		{
			printf("鼠标在窗口上停留(%d,%d)\n", e.ptMouse.x, e.ptMouse.y);
		}
		default:
			break;
		}
	};
	window->OnSize += [](IWindow* pWindow, MEvent &e)
	{
		MRect *rect = (MRect*)(e.lParam);
		printf("窗口更改（位置）大小(%d,%d,%d,%d)\n", rect->left, rect->top, GetRectWidth(*rect), GetRectHeight(*rect));
	};
	window->OnClose += [](IWindow *pWindow, MEvent &e) {PostQuitMessage(0); };
	window->Show();
	MSG msg;
	while (::GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}